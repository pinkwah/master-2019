#include <benchmark/benchmark.h>
#include <random>

#include <Eigen/Dense>
#include "KMeansClustering/KMeansClustering.h"
#include <real.hh>
#include <uti/helper.hh>
#include <uti/packet.hh>

#include <lloyd.hh>

constexpr size_t N = 10007;

template <class T, size_t N>
using dumb_vector = std::vector<std::array<T, N>>;


static double drand()
{
    static std::mt19937 g { std::random_device {}() };
    static std::uniform_real_distribution<> d;
    return d(g);
}

static double s_norm;

static const dumb_vector<double, 4>& testdata()
{
    static dumb_vector<double, 4> v {};
    static bool has_init {};

    if (has_init)
        return v;

    s_norm = 0;
    v.resize(N);
    for (auto& a : v) {
        for (auto& x : a) {
            x = drand();
            s_norm += x * x;
        }
    }

    has_init = true;
    return v;
}

static void bm_euclidean(benchmark::State& state)
{
    using vec = real::vec;
    using vector = real::set;

    const auto& input = testdata();

    auto common = vec::dim_type { input.front().size() };
    vector vs = common;
    for (const auto& a : input) {
        vec x = common;
        for (size_t i {}; i < a.size(); ++i) {
            x[i] = a[i];
        }
        vs.push_back(x);
    }

    for (auto _ : state) {
        vec v = common;
        for (const auto& x : vs) {
            v += x;
        }
        benchmark::DoNotOptimize(v /= static_cast<double>(vs.size()));
    }
}
BENCHMARK(bm_euclidean);

static void bm_eigen_colwise(benchmark::State& state)
{
    using vec = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using vector = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;

    const auto& input = testdata();
    auto dim = input.front().size();

    vector vs(dim, input.size());
    for (size_t j {}; j < input.size(); ++j) {
        const auto& a = input[j];
        vec x(dim);
        for (size_t i {}; i < a.size(); ++i) {
            x[i] = a[i];
        }
        vs.col(j) = x;
    }

    for (auto _ : state) {
        vec v(dim);
        for (size_t i {}; i < input.size(); ++i) {
            v += vs.col(i);
        }
        benchmark::DoNotOptimize(v /= static_cast<double>(vs.size()));
    }
}
BENCHMARK(bm_eigen_colwise);

static void bm_eigen_rowwise(benchmark::State& state)
{
    using vec = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using vector = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;

    const auto& input = testdata();
    auto dim = input.front().size();

    vector vs(input.size(), dim);
    for (size_t j {}; j < input.size(); ++j) {
        const auto& a = input[j];
        vec x(dim);
        for (size_t i {}; i < a.size(); ++i) {
            x[i] = a[i];
        }
        vs.row(j) = x;
    }

    for (auto _ : state) {
        vec v(dim);
        for (size_t i {}; i < input.size(); ++i) {
            v += vs.row(i);
        }
        benchmark::DoNotOptimize(v /= static_cast<double>(vs.size()));
    }
}
BENCHMARK(bm_eigen_rowwise);

static void bm_real_norm(benchmark::State& state)
{
    using vec = real::vec;
    using vector = real::set;

    const auto& input = testdata();

    auto common = vec::dim_type { input.front().size() };
    vector vs = common;
    for (size_t i {}; i < input.size(); ++i) {
        const auto& a = input[i];
        vec x = common;
        for (size_t j {}; j < a.size(); ++j) {
            x[j] = a[j];
        }
        vs.push_back(x);
    }
    fmt::print("{}\n", vs.size());

    for (auto _ : state) {
        benchmark::DoNotOptimize(vs.norm());
    }
}
BENCHMARK(bm_real_norm);

// extern "C" void bm_eigen_norm(benchmark::State& state)
// {
//     using vec = Eigen::Matrix<double, Eigen::Dynamic, 1>;
//     using vector = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;

//     const auto& input = testdata();
//     auto dim = input.front().size();

//     vector vs(input.size(), dim);
//     for (size_t j {}; j < input.size(); ++j) {
//         const auto& a = input[j];
//         vec x(dim);
//         for (size_t i {}; i < a.size(); ++i) {
//             x[i] = a[i];
//         }
//         vs.row(j) = x;
//     }

//     for (auto _ : state) {
//         benchmark::DoNotOptimize(vs.squaredNorm());
//     }
// }
// BENCHMARK(bm_eigen_norm);

void b_kmeans(benchmark::State& state)
{
    using vec = real::vec;
    using vector = real::set;

    const auto& input = testdata();

    auto common = vec::dim_type { input.front().size() };
    vector vs = common;
    for (const auto& a : input) {
        vec x = common;
        for (size_t i {}; i < a.size(); ++i) {
            x[i] = a[i];
        }
        vs.push_back(x);
    }

    for (auto _ : state) {
        lloyd::aux aux { 4, vs };
        lloyd::furthest_seed(aux, [] { return std::rand(); });
        while (!aux.converged()) {
            lloyd::perform(aux);
        }
    }
}

void b_KMeansCLustering(benchmark::State& state)
{
    using vec = Eigen::Matrix<double, Eigen::Dynamic, 1>;
    using vector = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>;

    const auto& input = testdata();
    auto dim = input.front().size();

    vector vs(input.size(), dim);
    for (size_t j {}; j < input.size(); ++j) {
        const auto& a = input[j];
        vec x(dim);
        for (size_t i {}; i < a.size(); ++i) {
            x[i] = a[i];
        }
        vs.row(j) = x;
    }

    for (auto _ : state) {
        KMeansClustering kmeans;
        kmeans.SetK(4);
        kmeans.SetPoints(vs);
        kmeans.SetInitMethod(KMeansClustering::KMEANSPP);
        kmeans.SetRandom(true);
        kmeans.Cluster();

        auto labels = kmeans.GetLabels();

        benchmark::DoNotOptimize(labels.back());
    }
}
BENCHMARK(b_KMeansCLustering);
