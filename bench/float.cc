#include <benchmark/benchmark.h>
#include <random>

namespace {
  template <class T>
  std::vector<T> s_generate(size_t n)
  {
      std::vector<T> output(n);
      for (auto& x : output)
          x = std::rand();
      return output;
  }

  template <class T>
  void b_generic(benchmark::State& state)
  {
      auto a = s_generate<T>(state.range(0));
      auto b = s_generate<T>(state.range(0));
      for (auto _ : state) {
          for (size_t i {}; i < a.size(); ++i) {
              benchmark::DoNotOptimize(a[i] * b[i]);
          }
      }
  }

  void b_float(benchmark::State& state)
  { b_generic<float>(state); }

  void b_double(benchmark::State& state)
  { b_generic<double>(state); }
}

BENCHMARK(b_float)->Ranges({{ 1 << 10, 1 << 20 }});
BENCHMARK(b_double)->Ranges({{ 1 << 10, 1 << 20 }});
