#!/usr/bin/env ruby

require 'optparse'
require_relative 'txt_format.rb'
require_relative 'lloyd.rb'

##
##
##
class RandomGaussian
  def initialize(mean, stddev, rand_helper = -> { Kernel.rand })
    @rand_helper = rand_helper
    @mean = mean
    @stddev = stddev
    @valid = false
    @next = 0
  end

  def rand
    if @valid
      @valid = false
      @next
    else
      @valid = true
      x, y = self.class.gaussian(@mean, @stddev, @rand_helper)
      @next = y
      x
    end
  end

  def self.rand(mean, stddev, rand_helper = -> { Kernel.rand })
    RandomGaussian.new(mean, stddev, rand_helper).rand
  end

  def self.gaussian(mean, stddev, rand)
    theta = 2 * Math::PI * rand.call
    rho = Math.sqrt(-2 * Math.log(1 - rand.call))
    scale = stddev * rho
    x = mean + scale * Math.cos(theta)
    y = mean + scale * Math.sin(theta)
    [x, y]
  end
end

##
##
##
class RandomDistribution
  def initialize(distribution)
    sum = distribution.sum
    @distribution = distribution.map.with_index { |x, i| [i, x / sum] }
  end

  def rand
    r = Kernel.rand
    @distribution.detect { |_, d| r -= d; r < 0 }.first
  end
end

##
## Options
##
OptStruct = Struct.new(*%i[output k d n s t g power], keyword_init: true)
OPTIONS = OptStruct.new(
  output: '/dev/stdout',
  k: 1,
  d: 1,
  n: 100,
  s: 100.0,
  t: 1.0,
  g: 1,
  power: 1
)

##
## Functions
##
def dist(a, b)
  a.zip(b).sum { |x, y| (x - y) ** 2 }
end

def gen_ct(d, s)
  Array.new(d) { (rand - 0.5) * s }
end

def gen_cts(d, s, k, t)
  cts = []
  while cts.length < k
    STDERR.printf '.'
    cts = [gen_ct(d, s)]
    100_000.times do
      break if cts.length >= k
      ct = gen_ct(d, s)
      cts << ct unless cts.any? { |c| dist(c, ct) < t }
    end
    return cts
  end
end

def gen_siz(d, k, t)
  Array.new(k) do
    Array.new(d) { t / 2.0 } # { RandomGaussian.rand(t / 4.0, t / 16.0) }
  end
end

def harmonic(n, s)
  (1..n).sum { |x| 1.0 / (x ** s) }
end

def random_distribution(k, g, s)
  h = harmonic(g, s)
  k = k.to_f
  g = g.to_f
  n = k / g
  RandomDistribution.new(Array.new(k) { |i| 1.0 / (n * (((i + 1)/n).ceil**s) * h) })
end

def main
  cts = gen_cts(d, s, k, t)
  cts_rand = cts.map { |ct| ct }.zip(gen_siz(d, k, t))
  cts_rand.map!(&:transpose)

  cts_dist = random_distribution(k, g, power)

  pts = Array.new(n) do
    ct = cts_rand[cts_dist.rand]
    ct.map { |x, r| RandomGaussian.rand(x, r) }
  end

  cts, cost = Lloyd.run(pts, cts)

  meta = {
    cts_dist: cts_dist.inspect,
    cost: cost,
    cts: cts
  }

  TxtFormat.write(output, pts, meta)
end

##
## Parse Options
##
OptionParser.new do |opts|
  opts.on('-o', '--output FILE', 'File to write to') { |s| OPTIONS.output = s }
  opts.on('-k', '--cen NUM', 'Number of centres') { |n| OPTIONS.k = n.to_i }
  opts.on('-d', '--dim NUM', 'Number of dimensions') { |n| OPTIONS.d = n.to_i }
  opts.on('-n', '--pts NUM', 'Number of points') { |n| OPTIONS.n = n.to_i }
  opts.on('-s', '--side NUM', 'Number of points') { |n| OPTIONS.s = n.to_f }
  opts.on('-t', '--dist NUM', 'Distance between clusters') { |n| OPTIONS.t = n.to_f }
  opts.on('-g', '--group NUM', 'Grouping') { |n| OPTIONS.g = n.to_f }
  opts.on('-S', '--power NUM', 'Power') { |n| OPTIONS.power = n.to_i }
end.parse!

OPTIONS.instance_eval { main }
