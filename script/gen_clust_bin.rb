#!/usr/bin/env ruby

gem 'ruby-statistics'

require 'statistics/distribution'
require 'optparse'
require_relative 'txt_format.rb'

##
## Options
##
OPTIONS = {
  output: '/dev/stdout',
  num_clusters: 1,
  d: 1,
  n: 100,
  r: nil,
  bool: false
}

##
## Functions
##
def generate_centroid
  coin = [0, 1]
  Array.new(OPTIONS[:d]) { coin.sample }
end

def generate_pt(ct)
  coin = [true] + Array.new(7, false) # p=0.25
  ct.map { |x| coin.sample ? 1 - x : x }
end

def dist(a, b, &blk)
  a.zip(b).map { |x, y| blk[x, y].to_s(2).count('1') }.sum
end

def compute(pts, cts, &blk)
  pts.sum { |pt| cts.map { |ct| dist(pt, ct, &blk) }.min }
end

def cluster(pts, cts, &blk)
  pts.map { |pt| cts.map.with_index { |ct, i| [dist(pt, ct, &blk), i] }.min.last }
end

def add(ct, pt)
  if OPTIONS[:bool]
    ct.map!.with_index { |x, i| (x + pt[i]) > 0 ? 1 : 0 }
  else
    ct.map!.with_index { |x, i| (x + pt[i]) % 2 }
  end
end

def main
  OPTIONS[:output] = "synth-k#{OPTIONS[:num_clusters]}-d#{OPTIONS[:d]}.txt" unless OPTIONS[:output]

  cts =
    if OPTIONS[:r]
      lr = Array.new(OPTIONS[:r]) { generate_centroid }
      Array.new(1 << OPTIONS[:r]) do |i|
        ct = Array.new(OPTIONS[:d], 0)
        OPTIONS[:r].times do |j|
          add(ct, lr[j]) if ((i >> j) & 1) == 1
        end
        ct
      end
    else
      Array.new(OPTIONS[:num_clusters]) { generate_centroid }
    end

  pts = Array.new(OPTIONS[:n]) { generate_pt(cts.sample) }
  nul = [Array.new(OPTIONS[:d], 0)]

  meta = {
    cts: cts,
    bin_cost: compute(pts, cts, &:^),
    bin_norm: compute(pts, nul, &:^),
    bin_clust: cluster(pts, cts, &:^),
    bool_cost: compute(pts, cts, &:|),
    bool_norm: compute(pts, nul, &:|),
    bool_clust: cluster(pts, cts, &:|)
  }

  meta[:bin_ncost] = meta[:bin_cost].to_f / meta[:bin_norm].to_f
  meta[:bool_ncost] = meta[:bool_cost].to_f / meta[:bool_norm].to_f

  TxtFormat.write(OPTIONS[:output], pts, meta)
end

##
## Parse Options
##
OptionParser.new do |opts|
  opts.on('-oFILE', '--output=FILE', 'File to write to') { |s| OPTIONS[:output] = s }
  opts.on('-kNUM', '--num-clusters=NUM', 'Number of clusters') { |n| OPTIONS[:num_clusters] = n.to_i }
  opts.on('-dNUM', '--num-dim=NUM', 'Number of dimensions') { |n| OPTIONS[:d] = n.to_i }
  opts.on('-nNUM', '--num-pts=NUM', 'Number of points') { |n| OPTIONS[:n] = n.to_i }
  opts.on('-rNUM', '--rank=NUM', 'Rank') { |n| OPTIONS[:r] = n.to_i }
  opts.on('-b', '--bool', 'Bool') { OPTIONS[:bool] = true }
end.parse!

main
