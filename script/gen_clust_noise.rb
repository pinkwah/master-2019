#!/usr/bin/env ruby

require 'optparse'
require_relative 'txt_format.rb'

##
## Options
##
OptStruct = Struct.new(*%i[output d n s sphere], keyword_init: true)
OPTIONS = OptStruct.new(
  output: '/dev/stdout',
  d: 1,
  n: 1,
  s: 1,
  sphere: false
)

##
## Functions
##
def polar(d, s)
  ang = Array.new(d - 1) { rand * 2 * Math::PI }
  ang << 0
  Array.new(d) do |i|
    sin = ang[0...i].map { |x| Math.sin(x) }
                  .inject(1, &:*)
    cos = Math.cos(ang[i])
    s * sin * cos
  end
end

def cart(d, s)
  Array.new(d) { (rand - 0.5) * s }
end

def main
  pts =
  if sphere
    Array.new(n) { polar(d, s) }
  else
    Array.new(n) { cart(d, s) }
  end
  TxtFormat.write(output, pts, OPTIONS.to_h)
end

##
## Parse Options
##
OptionParser.new do |opts|
  opts.on('-o', '--output FILE', 'File to write to') { |s| OPTIONS.output = s }
  opts.on('-d', '--dim NUM', 'Number of dimensions') { |n| OPTIONS.d = n.to_i }
  opts.on('-n', '--pts NUM', 'Number of points') { |n| OPTIONS.n = n.to_i }
  opts.on('-s', '--side NUM', 'Number of points') { |n| OPTIONS.s = n.to_i }
  opts.on('-c', '--sphere', 'Do a sphere') { |n| OPTIONS.sphere = n }
end.parse!

OPTIONS.instance_eval { main }
