#!/usr/bin/env ruby

require 'gnuplot'
require './txt_format.rb'

##
## Options
##
OptStruct = Struct.new(:input, :output, keyword_init: true)
OPTIONS = OptStruct.new(
  input: ARGV.shift,
  output: ARGV.shift
)

##
## Functions
##
def main
  raise ArgumentError, 'Input must be defined' unless input
  pts = TxtFormat.read_pts(input, &:to_f)
                 .map { |x| x[0..1] }

  Gnuplot.open do |gp|
    Gnuplot::Plot.new(gp) do |plot|
      if output
        plot.terminal 'png transparent size 2000,2000'
        plot.output output
      end

      plot.unset 'xtics'
      plot.unset 'xlabel'
      plot.unset 'ytics'
      plot.unset 'ylabel'
      plot.unset 'border'

      plot.data << Gnuplot::DataSet.new(pts.transpose) do |ds|
        ds.title = ''
        ds.using = '1:2 pt 7 ps 4 lc rgb "black"'
      end

      puts plot.to_gplot
    end
  end
end

##
## Parse Options
##

OPTIONS.instance_eval { main }
