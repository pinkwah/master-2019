#!/usr/bin/env ruby

require_relative 'txt_format.rb'

file = ARGV.shift
pp TxtFormat.read_meta(file)
