require 'json'

module TxtFormat
  def self.write(file, pts, meta = nil)
    File.open(file, 'w') do |f|
      f.puts(meta.to_json.split("\n").map { |l| '#! ' + l }.join("\n")) if meta

      pts.each do |pt|
        f.puts pt.join(' ')
      end
    end
  end

  def self.read_pts(file, &blk)
    pts = []
    File.read(file).each_line do |ln|
      next if ln[0] == '#'
      pts <<
        if blk
          ln.split.map(&blk)
        else
          ln.split.map(&:to_f)
        end
    end
    pts
  end

  def self.read_meta(file)
    JSON.load(File.read(file)
      .each_line
      .select { |l| l.start_with? '#! ' }
      .map { |l| l[3..-1] }
      .join("\n"))
  end
end
