#ifndef __BASIC_REF__85771540
#define __BASIC_REF__85771540

#include "mixin.hh"
#include "traits.hh"

namespace bin {
  template <size_t NBits, class AOp>
  struct mixin_types<basic_ref<NBits, AOp>> {
      using traits_type            = traits<NBits, AOp>;
      using dim_type               = typename traits_type::dim_type;
      using value_type             = unit;
      using pointer                = size_t*;
      using const_pointer          = const size_t*;
      using reference              = unit_ref<false>;
      using const_reference        = unit_ref<true>;
      using iterator               = unit_iter<false>;
      using const_iterator         = unit_iter<true>;
      using reverse_iterator       = std::reverse_iterator<iterator>;
      using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  };

  template <size_t NBits, class AOp>
  class basic_ref : public mixin<basic_ref<NBits, AOp>, NBits, AOp> {
  public:
      using traits_type            = typename mixin_types<basic_ref>::traits_type;
      using dim_type               = typename mixin_types<basic_ref>::dim_type;
      using value_type             = typename mixin_types<basic_ref>::value_type;
      using pointer                = typename mixin_types<basic_ref>::pointer;
      using const_pointer          = typename mixin_types<basic_ref>::const_pointer;
      using reference              = typename mixin_types<basic_ref>::reference;
      using const_reference        = typename mixin_types<basic_ref>::const_reference;
      using iterator               = typename mixin_types<basic_ref>::iterator;
      using const_iterator         = typename mixin_types<basic_ref>::const_iterator;
      using reverse_iterator       = typename mixin_types<basic_ref>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<basic_ref>::const_reverse_iterator;

  private:
      template <class, size_t, class> friend class mixin;

      dim_type m_dim;
      size_t *m_data;

  protected:
      /**
       * Data access
       */
      pointer p_data()
      { return m_data; }

      const_pointer p_data() const
      { return m_data; }

      dim_type p_dim() const
      { return m_dim; }

  public:
      basic_ref() = delete;

      basic_ref(const basic_ref& other) = default;

      /**
       * Private constructor
       */
      basic_ref(pointer data, dim_type dim):
          m_dim(dim),
          m_data(data)
      {}

      template <class U, typename = enable_if_vec_t<U>>
      basic_ref(U& other):
          m_dim(other.dim()),
          m_data(other.data())
      {}

      basic_ref& operator=(const basic_ref& other)
      {
          cassert_eq(this->nbits(), other.nbits());
          std::copy_n(other.data(), this->nbytes(), this->data());
          return *this;
      }

      template <class U, typename = enable_if_vec_t<U>>
      basic_ref& operator=(const U& other)
      {
          cassert_eq(this->nbits(), other.nbits());
          std::copy_n(other.data(), this->nbytes(), this->data());
          return *this;
      }

      /**
       * Reset
       */
      void reset(pointer data)
      { m_data = data; }

      void reset(pointer data, dim_type dim)
      {
          m_dim = dim;
          m_data = data;
      }
  };
}

#endif //__BASIC_REF__85771540
