#ifndef __BASIC_VEC__72876970
#define __BASIC_VEC__72876970

#include <boost/compressed_pair.hpp>
#include <memory>
#include <fmt/core.h>
#include "unit_iter.hh"
#include "mixin.hh"

namespace bin {
  template <size_t NBits, class AOp>
  struct mixin_types<basic_vec<NBits, AOp>> {
      using traits_type            = traits<NBits, AOp>;
      using dim_type               = typename traits_type::dim_type;
      using value_type             = unit;
      using pointer                = size_t*;
      using const_pointer          = const size_t*;
      using reference              = unit_ref<false>;
      using const_reference        = unit_ref<true>;
      using iterator               = unit_iter<false>;
      using const_iterator         = unit_iter<true>;
      using reverse_iterator       = std::reverse_iterator<iterator>;
      using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  };

  /**
   * binary::basic_vec container
   */
  template <size_t NBits, class AOp>
  class basic_vec : public mixin<basic_vec<NBits, AOp>, NBits, AOp> {
  public:
      using traits_type            = typename mixin_types<basic_vec>::traits_type;
      using dim_type               = typename mixin_types<basic_vec>::dim_type;
      using value_type             = typename mixin_types<basic_vec>::value_type;
      using pointer                = typename mixin_types<basic_vec>::pointer;
      using const_pointer          = typename mixin_types<basic_vec>::const_pointer;
      using reference              = typename mixin_types<basic_vec>::reference;
      using const_reference        = typename mixin_types<basic_vec>::const_reference;
      using iterator               = typename mixin_types<basic_vec>::iterator;
      using const_iterator         = typename mixin_types<basic_vec>::const_iterator;
      using reverse_iterator       = typename mixin_types<basic_vec>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<basic_vec>::const_reverse_iterator;

  private:
      template <class, size_t, class>
      friend class mixin;

      dim_type m_dim;
      std::array<size_t, nbytes(NBits)> m_data;

  protected:
      /**
       * Data access
       */
      pointer p_data()
      { return m_data.data(); }

      const_pointer p_data() const
      { return m_data.data(); }

      dim_type p_dim() const
      { return m_dim; }

  public:
      basic_vec() = delete;
      basic_vec(const basic_vec& other) = default;
      basic_vec(basic_vec&& other) = default;

      basic_vec(const dim_type &d):
          m_dim(d)
      { std::fill_n(this->data(), this->nbytes(), 0); }

      template <class U, typename = std::enable_if_t<is_bin_v<U>>>
      basic_vec(const U& other):
          m_dim(other.dim())
      { std::copy_n(other.data(), this->nbytes(), this->data()); }

      basic_vec(std::initializer_list<int> il):
          basic_vec(il.size())
      { std::copy(il.begin(), il.end(), this->begin()); }

      basic_vec(std::initializer_list<bool> il):
          basic_vec(il.size())
      { std::copy(il.begin(), il.end(), this->begin()); }

      basic_vec& operator=(const basic_vec& other)
      {
          assert(other.nbits() == this->nbits());
          std::copy(other.begin(), other.end(), this->begin());
          return *this;
      }

      basic_vec& operator=(basic_vec&&) = default;
  };

  template <class AOp>
  class basic_vec<0, AOp> : public mixin<basic_vec<0, AOp>, 0, AOp> {
  public:
      using traits_type            = typename mixin_types<basic_vec>::traits_type;
      using dim_type               = typename mixin_types<basic_vec>::dim_type;
      using value_type             = typename mixin_types<basic_vec>::value_type;
      using pointer                = typename mixin_types<basic_vec>::pointer;
      using const_pointer          = typename mixin_types<basic_vec>::const_pointer;
      using reference              = typename mixin_types<basic_vec>::reference;
      using const_reference        = typename mixin_types<basic_vec>::const_reference;
      using iterator               = typename mixin_types<basic_vec>::iterator;
      using const_iterator         = typename mixin_types<basic_vec>::const_iterator;
      using reverse_iterator       = typename mixin_types<basic_vec>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<basic_vec>::const_reverse_iterator;

  private:
      friend class mixin<basic_vec, 0, AOp>;

      dim_type m_dim;
      std::unique_ptr<size_t[]> m_data;

  protected:
      /**
       * Data access
       */
      pointer p_data()
      { return m_data.get(); }

      const_pointer p_data() const
      { return m_data.get(); }

      dim_type p_dim() const
      { return m_dim; }

  public:
      basic_vec() = delete;

      basic_vec(const basic_vec& other):
          m_dim(other.m_dim),
          m_data(std::make_unique<size_t[]>(this->nbytes()))
      { std::copy_n(other.data(), this->nbytes(), this->data()); }

      basic_vec(basic_vec&& other) = default;

      basic_vec(const dim_type& dim):
          m_dim(dim),
          m_data(std::make_unique<size_t[]>(this->nbytes()))
      { std::fill_n(this->data(), this->nbytes(), 0); }

      template <class U, typename = std::enable_if_t<is_bin_v<U>>>
      basic_vec(const U& other):
          m_dim(other.dim()),
          m_data(std::make_unique<size_t[]>(this->nbytes()))
      { std::copy_n(other.data(), this->nbytes(), this->data()); }

      basic_vec(std::initializer_list<int> il):
          basic_vec(il.size())
      { std::copy(il.begin(), il.end(), this->begin()); }

      basic_vec& operator=(const basic_vec& other)
      {
          if (other.nbytes() != this->nbytes()) {
              m_dim = other.dim();
              m_data = std::make_unique<size_t[]>(this->nbytes());
          }
          std::copy_n(other.data(), this->nbytes(), this->data());
          return *this;
      }

      template <class U, typename = std::enable_if_t<is_bin_v<U>>>
      basic_vec& operator=(const U& other)
      {
          std::copy_n(other.data(), this->nbytes(), this->data());
          return *this;
      }
  };
}

#endif //__BASIC_VEC__72876970
