#ifndef __BASIC_VIEW__85771540
#define __BASIC_VIEW__85771540

#include "mixin.hh"
#include "traits.hh"

namespace bin {
  template <size_t NBits, class AOp>
  struct mixin_types<basic_view<NBits, AOp>> {
      using traits_type            = traits<NBits, AOp>;
      using dim_type               = typename traits_type::dim_type;
      using value_type             = unit;
      using pointer                = const size_t*;
      using const_pointer          = const size_t*;
      using reference              = unit_ref<true>;
      using const_reference        = unit_ref<true>;
      using iterator               = unit_iter<true>;
      using const_iterator         = unit_iter<true>;
      using reverse_iterator       = std::reverse_iterator<iterator>;
      using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  };

  template <size_t NBits, class AOp>
  class basic_view : public mixin<basic_view<NBits, AOp>, NBits, AOp> {
  public:
      using traits_type            = typename mixin_types<basic_view>::traits_type;
      using dim_type               = typename mixin_types<basic_view>::dim_type;
      using value_type             = typename mixin_types<basic_view>::value_type;
      using pointer                = typename mixin_types<basic_view>::pointer;
      using const_pointer          = typename mixin_types<basic_view>::const_pointer;
      using reference              = typename mixin_types<basic_view>::reference;
      using const_reference        = typename mixin_types<basic_view>::const_reference;
      using iterator               = typename mixin_types<basic_view>::iterator;
      using const_iterator         = typename mixin_types<basic_view>::const_iterator;
      using reverse_iterator       = typename mixin_types<basic_view>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<basic_view>::const_reverse_iterator;

  private:
      template <class, size_t, class> friend class mixin;

      dim_type m_dim;
      const size_t *m_data;

  protected:
      /**
       * Data access
       */
      pointer p_data()
      { return m_data; }

      const_pointer p_data() const
      { return m_data; }

      dim_type p_dim() const
      { return m_dim; }

  public:
      basic_view() = delete;

      basic_view(const basic_view& other) = default;
      basic_view(basic_view&) = default;

      /**
       * Private constructor
       */
      basic_view(pointer data, dim_type dim):
          m_dim(dim),
          m_data(data)
      {}

      template <class U>
      basic_view(const U& other):
          m_dim(other.dim()),
          m_data(other.data())
      {}

      basic_view& operator=(const basic_view&) = delete;
      basic_view& operator=(basic_view&&) = delete;

      /**
       * Reset
       */
      void reset(pointer data)
      { m_data = data; }

      void reset(pointer data, dim_type dim)
      {
          m_dim = dim;
          m_data = data;
      }
  };
}

#endif //__BASIC_VIEW__85771540
