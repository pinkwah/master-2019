#ifndef __DISTANCE__22325226
#define __DISTANCE__22325226

#include <cstddef>

template <class A, class B>
size_t dist_sq(const A& a, const B& b)
{
    assert(a.dim() == b.dim());
    return (a - b).popcount();
}

#endif //__DISTANCE__22325226
