#ifndef __FORMAT__15355928
#define __FORMAT__15355928

#include <fmt/core.h>
#include "fwd.hh"

/**
 * Formatting routine
 */
template <bool Const>
struct fmt::formatter<::bin::unit_ref<Const>> {
    template <class ParseContext>
        constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
        auto format(const ::bin::unit_ref<Const>& v, FormatContext& ctx)
    {
        return format_to(ctx.begin(), "{}", static_cast<int>(v));
    }
};

template <size_t NBits, class AOp>
struct fmt::formatter<::bin::basic_vec<NBits, AOp>> {
    using basic_vec = ::bin::basic_vec<NBits, AOp>;

    template <class ParseContext>
        constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
        auto format(const basic_vec& v, FormatContext& ctx)
    {
        auto it = format_to(ctx.begin(), "{{ ");
        for (size_t i {}; i + 1 < v.dim(); ++i) {
            it = format_to(it, "{}, ", v[i]);
        }
        return format_to(it, "{} }}", v.back());
    }
};

template <size_t NBits, class AOp>
struct fmt::formatter<::bin::basic_ref<NBits, AOp>> {
    using basic_vec = ::bin::basic_ref<NBits, AOp>;

    template <class ParseContext>
        constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
        auto format(const basic_vec& v, FormatContext& ctx)
    {
        auto it = format_to(ctx.begin(), "{{ ");
        for (size_t i {}; i + 1 < v.dim(); ++i) {
            it = format_to(it, "{}, ", v[i]);
        }
        return format_to(it, "{} }}", v.back());
    }
};

#endif //__FORMAT__15355928
