#ifndef __UNIT_ITER__86546449
#define __UNIT_ITER__86546449

#include "unit_ref.hh"

namespace bin {
  template <bool Const = false>
  class unit_iter {
      unit_ref<Const> m_ref;

  public:
      using value_type        = unit_ref<false>;
      using reference         = unit_ref<false>;
      using difference_type   = std::ptrdiff_t;
      using const_reference   = unit_ref<true>;
      using pointer           = unit_ref<false>*;
      using const_pointer     = unit_ref<true>*;
      using iterator_category = std::forward_iterator_tag;

      unit_iter(size_t *ptr, size_t off):
          m_ref(ptr, off)
      {}

      unit_ref<Const>& operator*()
      { return m_ref; }

      const unit_ref<Const>& operator*() const
      { return m_ref; }

      unit_ref<Const>* operator->()
      { return &m_ref; }

      const unit_ref<Const>* operator->() const
      { return &m_ref; }

      unit_iter& operator++()
      {
          ++m_ref.m_off;
          if (m_ref.m_off == sizeof(size_t)) {
              m_ref.m_off = 0;
              ++m_ref.m_ptr;
          }
          return *this;
      }

      bool operator==(const unit_iter& iter) const
      { return (m_ref.m_ptr == iter.m_ref.m_ptr && m_ref.m_off == iter.m_ref.m_off); }

      bool operator!=(const unit_iter& iter) const
      { return !(*this == iter); }
  };

  template <>
  class unit_iter<true> {
      unit_ref<true> m_ref;

  public:
      using value_type        = unit_ref<true>;
      using reference         = unit_ref<true>;
      using difference_type   = std::ptrdiff_t;
      using const_reference   = unit_ref<true>;
      using pointer           = unit_ref<true>*;
      using const_pointer     = unit_ref<true>*;
      using iterator_category = std::forward_iterator_tag;

      unit_iter(const size_t *ptr, size_t off):
              m_ref(ptr, off)
      {}

      const unit_ref<true>& operator*() const
      { return m_ref; }

      const unit_ref<true>* operator->() const
      { return &m_ref; }

      unit_iter& operator++()
      {
          ++m_ref.m_off;
          if (m_ref.m_off == sizeof(size_t)) {
              m_ref.m_off = 0;
              ++m_ref.m_ptr;
          }
          return *this;
      }

      unit_iter operator+(difference_type i) const
      {
          if (i < 0)
              return (*this - (-i));

          auto c = *this;
          c.m_ref.m_ptr += i / sizeof(size_t);
          c.m_ref.m_off += i % sizeof(size_t);
          if (c.m_ref.m_off >= sizeof(size_t)) {
              ++c.m_ref.m_ptr;
              c.m_ref.m_off -= sizeof(size_t);
          }
          return c;
      }

      unit_iter operator-(difference_type i) const
      {
          std::abort();
      }

      bool operator==(const unit_iter& iter) const
      { return (m_ref.m_ptr == iter.m_ref.m_ptr && m_ref.m_off == iter.m_ref.m_off); }

      bool operator!=(const unit_iter& iter) const
      { return !(*this == iter); }
  };
}

#endif //__UNIT_ITER__86546449
