#ifndef __UNIT_REF__62193964
#define __UNIT_REF__62193964

#include <istream>
#include "unit.hh"

namespace bin {
  namespace detail {
    constexpr void bit_xor(int val, size_t &dst_ptr, size_t dst_off)
    { dst_ptr ^= (val << dst_off); }

    constexpr void bit_and(int val, size_t &dst_ptr, size_t dst_off)
    { dst_ptr &= (val << dst_off); }

    constexpr void bit_assign(int val, size_t &dst_ptr, size_t dst_off)
    {
        auto dst_bit = 1 << dst_off;
        dst_ptr = (~dst_bit & dst_ptr) | (val << dst_off);
    }
  }

  template <bool>
  class unit_iter;

  template <bool Const = false>
  class unit_ref {
      size_t *m_ptr {};
      size_t m_off {};

      template <bool> friend class unit_iter;

  public:
      constexpr unit_ref() = delete;
      constexpr unit_ref(const unit_ref&) = default;

      constexpr explicit unit_ref(size_t *ptr, size_t off):
          m_ptr(ptr),
          m_off(off)
      {}

      constexpr unit_ref& operator=(const unit& ref)
      {
          detail::bit_assign(static_cast<int>(ref), *m_ptr, m_off);
          return *this;
      }

      template <bool C>
      constexpr unit_ref& operator=(const unit_ref<C>& ref)
      {
          detail::bit_assign(static_cast<int>(ref), *m_ptr, m_off);
          return *this;
      }

      constexpr unit_ref& operator=(int val)
      {
          assert(val == 0 || val == 1);
          detail::bit_assign(val, *m_ptr, m_off);
          return *this;
      }

      constexpr operator int() const
      { return (*m_ptr >> m_off) & 1; }

      constexpr unit_ref& operator+=(const unit_ref& ref)
      {
          detail::bit_xor(static_cast<int>(ref), *m_ptr, m_off);
          return *this;
      }

      constexpr unit_ref& operator-=(const unit_ref& x)
      { detail::bit_xor(static_cast<int>(x), *m_ptr, m_off); return *this; }
      constexpr unit_ref& operator-=(const unit& x)
      { detail::bit_xor(static_cast<int>(x), *m_ptr, m_off); return *this; }

      constexpr unit_ref& operator*=(const unit_ref& ref)
      {
          detail::bit_and(static_cast<int>(ref), *m_ptr, m_off);
          return *this;
      }

      constexpr unit_ref& operator/=(const unit_ref& ref)
      {
          assert(static_cast<int>(ref) != 0);
          return *this;
      }

      constexpr unit_ref& operator%=(const unit_ref& ref)
      {
          assert(static_cast<int>(ref) != 0);
          detail::bit_assign(0, *m_ptr, m_off);
          return *this;
      }
  };

  template <>
  class unit_ref<true> {
      const size_t *m_ptr {};
      size_t m_off {};

      template <bool> friend class unit_iter;

  public:
      constexpr unit_ref() = default;
      constexpr unit_ref(const unit_ref&) = default;

      constexpr explicit unit_ref(const size_t *ptr, size_t off):
              m_ptr(ptr),
              m_off(off)
      {}

      constexpr operator int() const
      { return (*m_ptr >> m_off) & 1; }
  };

  template <bool A, bool B>
  constexpr bool operator==(const unit_ref<A>& a, const unit_ref<B>& b)
  { return static_cast<int>(a) == static_cast<int>(b); }
  template <bool C>
  constexpr bool operator==(const unit& a, const unit_ref<C>& b)
  { return static_cast<int>(a) == static_cast<int>(b); }
  template <bool C>
  constexpr bool operator==(const unit_ref<C>& a, const unit& b)
  { return static_cast<int>(a) == static_cast<int>(b); }

  template <bool A, bool B>
  constexpr bool operator!=(const unit_ref<A>& a, const unit_ref<B>& b)
  { return static_cast<int>(a) != static_cast<int>(b); }
  template <bool C>
  constexpr bool operator!=(const unit& a, const unit_ref<C>& b)
  { return static_cast<int>(a) != static_cast<int>(b); }
  template <bool C>
  constexpr bool operator!=(const unit_ref<C>& a, const unit& b)
  { return static_cast<int>(a) != static_cast<int>(b); }

  template <bool A, bool B>
  constexpr bool operator<(const unit_ref<A>& a, const unit_ref<B>& b)
  { return static_cast<int>(a) < static_cast<int>(b); }
  template <bool C>
  constexpr bool operator<(const unit& a, const unit_ref<C>& b)
  { return static_cast<int>(a) < static_cast<int>(b); }
  template <bool C>
  constexpr bool operator<(const unit_ref<C>& a, const unit& b)
  { return static_cast<int>(a) < static_cast<int>(b); }

  template <bool A, bool B>
  constexpr bool operator>(const unit_ref<A>& a, const unit_ref<B>& b)
  { return static_cast<int>(a) > static_cast<int>(b); }
  template <bool C>
  constexpr bool operator>(const unit& a, const unit_ref<C>& b)
  { return static_cast<int>(a) > static_cast<int>(b); }
  template <bool C>
  constexpr bool operator>(const unit_ref<C>& a, const unit& b)
  { return static_cast<int>(a) > static_cast<int>(b); }

  template <bool A, bool B>
  constexpr bool operator<=(const unit_ref<A>& a, const unit_ref<B>& b)
  { return static_cast<int>(a) <= static_cast<int>(b); }
  template <bool C>
  constexpr bool operator<=(const unit& a, const unit_ref<C>& b)
  { return static_cast<int>(a) <= static_cast<int>(b); }
  template <bool C>
  constexpr bool operator<=(const unit_ref<C>& a, const unit& b)
  { return static_cast<int>(a) <= static_cast<int>(b); }

  template <bool A, bool B>
  constexpr bool operator>=(const unit_ref<A>& a, const unit_ref<B>& b)
  { return static_cast<int>(a) >= static_cast<int>(b); }
  template <bool C>
  constexpr bool operator>=(const unit& a, const unit_ref<C>& b)
  { return static_cast<int>(a) >= static_cast<int>(b); }
  template <bool C>
  constexpr bool operator>=(const unit_ref<C>& a, const unit& b)
  { return static_cast<int>(a) >= static_cast<int>(b); }

  template <bool C>
  constexpr unit operator!(const unit_ref<C>& a)
  { return unit { !static_cast<int>(a) }; }

  template <bool C>
  constexpr unit operator+(const unit_ref<C>& a)
  { return unit { static_cast<int>(a) }; }

  template <bool C>
  constexpr unit operator-(const unit_ref<C>& a)
  { return unit { static_cast<int>(a) }; }

  template <bool A, bool B>
  constexpr unit operator+(const unit_ref<A>& a, const unit_ref<B>& b)
  { return unit { static_cast<int>(a) ^ static_cast<int>(b) }; }
  template <bool C>
  constexpr unit operator+(const unit& a, const unit_ref<C>& b)
  { return unit { static_cast<int>(a) ^ static_cast<int>(b) }; }
  template <bool C>
  constexpr unit operator+(const unit_ref<C>& a, const unit& b)
  { return unit { static_cast<int>(a) ^ static_cast<int>(b) }; }

  template <bool A, bool B>
  constexpr unit operator-(const unit_ref<A>& a, const unit_ref<B>& b)
  { return unit { static_cast<int>(a) ^ static_cast<int>(b) }; }
  template <bool C>
  constexpr unit operator-(const unit& a, const unit_ref<C>& b)
  { return unit { static_cast<int>(a) ^ static_cast<int>(b) }; }
  template <bool C>
  constexpr unit operator-(const unit_ref<C>& a, const unit& b)
  { return unit { static_cast<int>(a) ^ static_cast<int>(b) }; }

  /** a ✕ b = 1 iff. both a and b = 1. 0 otherwise. */
  template <bool A, bool B>
  constexpr unit operator*(const unit_ref<A>& a, const unit_ref<B>& b)
  { return unit { static_cast<int>(a) & static_cast<int>(b) }; }
  template <bool C>
  constexpr unit operator*(const unit& a, const unit_ref<C>& b)
  { return unit { static_cast<int>(a) & static_cast<int>(b) }; }
  template <bool C>
  constexpr unit operator*(const unit_ref<C>& a, const unit& b)
  { return unit { static_cast<int>(a) & static_cast<int>(b) }; }

  /** a / b is only defined if b ≠ 0. Since it's UB to divide by zero, we can
   * simply return a */
  template <bool A, bool B>
  constexpr unit operator/(const unit_ref<A>& a, const unit_ref<B>& b)
  { assert(static_cast<int>(b) != 0); return unit { static_cast<int>(a) }; }
  template <bool C>
  constexpr unit operator/(const unit& a, const unit_ref<C>& b)
  { assert(static_cast<int>(b) != 0); return unit { static_cast<int>(a) }; }
  template <bool C>
  constexpr unit operator/(const unit_ref<C>& a, const unit& b)
  { assert(static_cast<int>(b) != 0); return unit { static_cast<int>(a) }; }

  /** a % b is only defined if b ≠ 0, and both 0 and 1 = 0 (mod 1), simply return 0. */
  template <bool A, bool B>
  constexpr unit operator%(const unit_ref<A>&, const unit_ref<B>& b)
  { assert(static_cast<int>(b) != 0); return {}; }
  template <bool C>
  constexpr unit operator%(const unit&, const unit_ref<C>& b)
  { assert(static_cast<int>(b) != 0); return {}; }
  template <bool C>
  constexpr unit operator%(const unit_ref<C>&, const unit& b)
  { assert(static_cast<int>(b) != 0); return {}; }
}

inline std::istream& operator>>(std::istream& s, bin::unit_ref<false> r)
{
    int i {};
    s >> i;
    r = i & 1;
    return s;
}

namespace std {
  template <bool Const>
  ::bin::unit_ref<Const> round(const ::bin::unit_ref<Const>&x)
  { return x; }
}

#endif //__UNIT_REF__62193964
