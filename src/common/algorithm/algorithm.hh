#ifndef __ALGORITHM__64091307
#define __ALGORITHM__64091307

#include "compute_cost.hh"
#include "discretize.hh"
#include "round.hh"

namespace common {
  cc::unit project(const cc::set& pts, cc::set& cts, size_t rank);
  cc::unit project_bool(const cc::set& pts, cc::set& cts, size_t rank);
}

#endif //__ALGORITHM__64091307
