#ifndef __ROUND__52718946
#define __ROUND__52718946

#include "compute_cost.hh"
#include <bin/fwd.hh>

namespace common {
  inline auto round(const cc::set& pts, cc::set& clusters);
}

inline auto common::round(const cc::set& pts, cc::set& clusters)
{
#ifndef CC_IS_BIN
    for (auto& ct : clusters) {
        for (auto& x : ct) {
            x = std::round(x);
            x = std::clamp(x, cc::unit(0), cc::unit(1));
        }
    }
#endif

    return compute_cost(pts, clusters);
}

#endif //__ROUND__52718946
