#ifndef __FORMAT__55020820
#define __FORMAT__55020820

#include "util/filesystem.hh"
#include <config.hh>

namespace format {
  cc::set load(const path& path, size_t max_dim, size_t max_pts);
}

#endif //__FORMAT__55020820
