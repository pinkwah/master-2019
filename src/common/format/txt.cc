#include <fstream>
#include <sstream>
#include "txt.hh"

using namespace format;

namespace {
  size_t s_dim(std::istream& f)
  {
      /* ignore comments */
      while (!f.eof()) {
          if (f.peek() != '#')
              break;
          f.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      }

      std::string ln;
      std::getline(f, ln);
      std::istringstream iss(ln);

      size_t size {};
      while (!iss.eof()) {
          double t {};
          iss >> t;
          ++size;
      }

      return size;
  }

  cc::set s_read(std::istream& s, size_t dim, size_t max_pts)
  {
      cc::set vector(dim);
      for (; vector.size() < max_pts && !s.eof();) {
          /* ignore comments */
          if (s.peek() == '#') {
              s.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
              continue;
          }

          cc::vec vec(dim);
          for (size_t j {}; j < dim; ++j) {
              s >> vec[j];
              if (s.eof()) {
                  return vector;
              }
          }

          /* Read until next line */
          s.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

          vector.push_back(vec);
      }

      return vector;
  }
}

//
// Txt::test
//
bool Txt::test(const path& path)
{
    return path.extension() == ".txt";
}

//
// Txt::load
//
cc::set Txt::read(const path& path, size_t max_dim, size_t max_pts)
{
    std::ifstream f(path.c_str());
    if (!f.is_open()) {
        throw std::runtime_error { "Couldn't open file for reading" };
    }

    auto dim = std::min(s_dim(f), max_dim);
    f.seekg(0, f.beg);
    return s_read(f, dim, max_pts);
}
