#include <regex>
#include <fmt/core.h>
#include <fmt/ostream.h>

#include "options.hh"

namespace po = boost::program_options;
using fmt::print;

namespace {
  void s_print_help(std::string_view exec, po::options_description &desc)
  {
      print("Syntax: {} [OPTION...] INPUT [SOLUTION]", exec);
      print("{}", desc);
  }

  common::options::duration_type s_parse_duration(const std::string &text)
  {
      static std::regex regex("([[:digit:]]+)(?::([[:digit:]]))?([hms])?",
                              std::regex::ECMAScript | std::regex::optimize);

      std::smatch match;
      std::regex_match(text, match, regex);
      assert(!match.empty());

      std::ssub_match sub1 = match[1];
      std::ssub_match sub2 = match[2];
      std::ssub_match sub3 = match[3];

      auto m1 = sub1.matched ? std::stoul(sub1.str()) : 0;
      auto m2 = sub2.matched ? std::stoul(sub2.str()) : 0;
      auto m3 = sub3.str();

      if (m3.empty() || m3 == "s") {
          return std::chrono::seconds{m1};
      } else if (m3 == "m") {
          return std::chrono::minutes{m1} + std::chrono::seconds{m2};
      } else if (m3 == "h") {
          return std::chrono::hours{m1} + std::chrono::minutes{m2};
      }

      return {};
  }

  void s_parse_args(int argc, char **argv, const po::options_description *custom_desc)
  {
      std::string exact_file;
      auto &opts = common::g_options;

      po::options_description desc("Common options");
      desc.add_options()
          ("help", "Produce help message")
          ("no-points", po::bool_switch(&opts.no_points), "Don't output clusters")
          ("verify", po::bool_switch(&opts.verify), "Sanity-check after every trial")
          ("dynamic", po::bool_switch(&opts.dynamic), "Use the unsized version of vector")
          ("discrete", po::bool_switch(&opts.discrete), "Require that C ⊂ P")
          ("round", po::bool_switch(&opts.round), "Require that C ⊂ 𝐙ⁿ")
          ("project", po::bool_switch(&opts.project), "Use projection")
          ("project-bool", po::bool_switch(&opts.project_bool), "Use projection bool")
          ("pre", po::bool_switch(&opts.pre), "Preprocess")
          ("input", po::value(&opts.input_file), "Input file")
          ("solution", po::value(&exact_file), "File containing the exact solution")
          ("type", po::value(&opts.type), "C++ type to use")
          ("cost", po::value(&opts.cost), "Cost function to use")
          (",k", po::value(&opts.num_clusters), "Number of clusters")
          ("rank,r", po::value(&opts.rank), "Rank of output")
          ("threads", po::value(&opts.num_threads), "Number of threads to use")
          ("trials,t", po::value(&opts.num_trials), "Number of trials to run")
          ("timeout", po::value<std::string>(), "Timeout")
          (",d", po::value(&opts.max_dim), "Maximum dimension")
          (",n", po::value(&opts.max_pts), "Maximum number of points");

      po::positional_options_description pos{};
      pos.add("input", 1).add("solution", 2);

      po::options_description command_line{};
      if (custom_desc) {
          command_line.add(*custom_desc);
      }
      command_line.add(desc);

      po::variables_map vm{};
      po::store(
          po::command_line_parser(argc, argv)
          .options(command_line)
          .positional(pos)
          .run(),
          vm);
      po::notify(vm);

      if (vm.count("help")) {
          s_print_help(argv[0], command_line);
          std::exit(EXIT_FAILURE);
      }

      if (vm.count("timeout")) {
          opts.timeout = s_parse_duration(vm["timeout"].as<std::string>());
      }

      if (!vm.count("input")) {
          s_print_help(argv[0], command_line);
          std::exit(EXIT_FAILURE);
      }

      if (vm.count("solution")) {
          opts.exact_file = vm["solution"].as<std::string>();
      }
  }

  void s_setup(int argc, char **argv, const po::options_description *custom_desc)
  {
      /* Copy arguments to g_args */
      for (int i {}; i < argc; ++i) {
          common::g_args.push_back(argv[i]);
      }

      /* Send default stdout stuff to stderr */
      stdout = fdopen(STDERR_FILENO, "w");
      common::g_result_fd = fdopen(STDOUT_FILENO, "w");

      /* Parse arguments */
      s_parse_args(argc, argv, custom_desc);
  }

} // namespace

//
// Global variables
//
std::vector<std::string_view> common::g_args;
common::options common::g_options{};
std::FILE *common::g_result_fd;
thread_local std::mt19937 common::g_gen { std::random_device {}() };
thread_local size_t common::g_thread_id {};
thread_local size_t common::g_trial_id {};

//
//
//
void common::setup(int argc, char **argv)
{
    s_setup(argc, argv, nullptr);
}

//
// setup
//
void common::setup(int argc, char **argv, const boost::program_options::options_description &custom_desc)
{
    s_setup(argc, argv, &custom_desc);
}
