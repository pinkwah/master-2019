#include <codecvt>
#include "yaml.hh"

using namespace yaml;

namespace {
  constexpr size_t s_mul = 2;

  // convert UTF-8 string to wstring
  std::wstring utf8_to_wstring (std::string_view str)
  {
      std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
      return myconv.from_bytes(str.begin(), str.end());
  }

  // convert wstring to UTF-8 string
  std::string wstring_to_utf8 (const std::wstring& str)
  {
      std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
      return myconv.to_bytes(str);
  }
}

std::string yaml::safe_string(std::string_view in_str)
{
    constexpr std::array<std::pair<wchar_t, std::wstring_view>, 6> convert {{
            { L'\t', L"\\t" },
            { L'\n', L"\\n" },
            { L'\r', L"\\r" },
            { L'\'', L"\\'" },
            { L'"', L"\\\"" },
            { L'\\', L"\\\\" }
        }};

    auto str = utf8_to_wstring(in_str);

    std::wstring safe;
    safe.push_back('"');
    for (auto chr : str) {
        bool found_conv {};
        for (const auto& conv : convert) {
            if (chr == conv.first) {
                safe.append(conv.second);
                found_conv = true;
                break;
            }
        }
        if (found_conv) continue;

        /* Non-printable character get their output converted to hex */
        if (!std::iswprint(chr)) {
            char hex[] { '\\', 'U', 0, 0, 0, 0, 0, 0, 0, 0 };
            for (size_t i = 9; i >= 2; --i) {
                auto ch = chr & 0xf;
                if (ch <= 9) {
                    hex[i] = '0' + ch;
                } else {
                    hex[i] = 'a' + ch - 10;
                }
                chr >>= 4;
            }
            safe.append(std::begin(hex), std::end(hex));
            continue;
        }

        safe.push_back(chr);
    }
    safe.push_back('"');

    return wstring_to_utf8(safe);
}

//
// yaml::writer::writer
//
writer::writer(std::FILE *fd, size_t indent):
    m_fd(fd),
    m_indent(indent)
{
}

//
// yaml::writer::~writer
//
writer::~writer()
{
}

//
// yaml::writer::indent
//
writer writer::indent() const
{
    return { m_fd, m_indent + 1 };
}

//
// yaml::writer::write_prefix
//
void writer::write_prefix() const
{
    for (size_t i {}; i < s_mul * m_indent; ++i) {
        fputc(' ', m_fd);
    }
}

//
// yaml::writer::write
//
void writer::write(std::string_view str) const
{
    fwrite(str.data(), 1, str.size(), m_fd);
}

//
// yaml::array::array
//
array::array(const writer& writer):
    m_writer(writer)
{
}

//
// yaml::array::~array
//
array::~array()
{
}

//
// yaml::array::subarray
//
array array::subarray() const
{
    m_writer.write_prefix();
    m_writer("-\n");
    return m_writer.indent();
}

//
// yaml::array::subdict
//
dict array::subdict() const
{
    m_writer.write_prefix();
    m_writer("-\n");
    return m_writer.indent();
}

//
// yaml::dict::dict
//
dict::dict(const writer& writer):
    m_writer(writer)
{
}

//
// yaml::dict::~dict
//
dict::~dict()
{
}

//
// yaml::dict::subarray
//
array dict::subarray(std::string_view key) const
{
    m_writer.write_prefix();
    m_writer(fmt::format("{}:\n", key));
    return m_writer.indent();
}

//
// yaml::dict::subdict
//
dict dict::subdict(std::string_view key) const
{
    m_writer.write_prefix();
    m_writer(fmt::format("{}:\n", key));
    return m_writer.indent();
}
