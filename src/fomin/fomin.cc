#include <string_view>
#include <string>
#include <queue>

#include "common/start.hh"
#include "common/cost.hh"
#include <util/constraints.hh>
#include <util/subset.hh>

using namespace std::string_view_literals;

namespace {
  size_t s_lloyd {};
  double s_epsilon = 1;

  struct state {
      cc::set clusters;
      cc::set points;
      double delta {};

      state(const cc::set& points):
          clusters(points.dim()),
          points(points)
      {}

      state(const cc::set& clusters, const cc::set& points, double delta):
          clusters(clusters),
          points(points),
          delta(delta)
      {}

      state(const state&) = default;
      state(state&&) = default;

      state& operator=(const state&) = default;
      state& operator=(state&&) = default;
  };

  //
  // Fomin
  //
  class s_fomin : common::algorithm<s_fomin> {
  public:
      cc::result trial(const cc::set& points) override {
          cc::set empty(points.dim());
          cc::result res;

          uti::constraints cons { common::g_options.num_clusters, points.dim() };

          /* 1. P <- {(∅, X, 0)} */
          std::queue<state> p;
          p.emplace(points);

          /* 2. δ' <- ε/40k*, α <- δ'/(5^k - 1), β <- (a/k)^(k-1) * δ'/2k(1 + δ'), ε <- α/7, δ <- α/7. */
          const auto k = common::g_options.num_clusters;
          const auto k_star = k;
          const auto delta_ = s_epsilon / (40 * k_star);
          const auto alpha = delta_ / (std::pow(5, k) - 1);
          const auto beta = std::pow(alpha / k, k - 1) * delta_ / (2*k*(1 + delta_));
          const auto epsilon = alpha / 7;
          const auto delta = alpha / 7;

          /* 3. for (C1, S, δ1) ∈ P such that |C1| < k and S ≠ ∅ do */
          while (!p.empty()) {
              /* 4. P <- P \ {(C1, S, δ1)} */
              auto [cluster_1, order, delta_1] = p.front();
              p.pop();

              /* 5. Let π be a linear order of S according to the non-decreasing  */
              std::sort(order.begin(), order.end());

              /* 6. Let S' be the last ⌈|S|/2⌉ vectors in the order π. */
              auto sfirst = order.begin();
              auto slast = sfirst + (order.size() + 1) / 2;

              /* 7. P <- P ∪ {(C1, S', δ1)} */
              // p.emplace(cluster_1, cc::set { sfirst, slast }, delta_1);

              /* 8. Guess I ∈ ([k] C |C1|) such that:
               * (i) < C1, proj_I(R) > (C satisfies R)
               * (ii) there is a good δ1-extension C2 of (C1, X \ S)
               * (iii) < C2, R(I, C1) > */
              size_t indices;

              /* 9. R' <- R(I, C1) */
              auto cons_prime = cons.restrict(indices, cluster_1);

              /* 10. l <- k - |C1| */
              auto remainder = common::g_options.num_clusters - cluster_1.size();
              std::vector<size_t> remainder_set(remainder + 1, 0);
              std::iota(remainder_set.begin(), remainder_set.end(), 0);

              /* 11. k' ∈ { 1, ..., k - |C1| } and I' ∈ ([l] C k') */
              for (size_t k_ {}; k_ + cluster_1.size() < common::g_options.num_clusters + 1; ++k_) {
                  for (const auto& x : uti::subset { remainder_set.begin(), remainder_set.end(), k_ } /* [l] C k' */) {
                      auto h = std::pow(common::g_options.num_clusters / alpha, remainder);

                      /* 12. Guess k' integers w'1 ... w'k, such that [[stuff]] */

                  }
              }
          }

          return res;
      }

      std::string_view name() const override
      { return "fomin"sv; }
  };

  //
  //
  //
  // void s_writer(const yaml::dict& y)
  // {
  //     y.value("lloyd_iterations", s_lloyd);
  // }
}

const std::string_view g_algorithm = "fomin"sv;

//
// main
//
int main(int argc, char **argv)
{
    namespace po = boost::program_options;

    po::options_description desc("Fomin options");
    desc.add_options()
        ("lloyd,l", po::value(&s_lloyd), "Number of iterations of Lloyd's to run after");

    common::setup(argc, argv, desc);
    common::start(s_fomin {});
}
