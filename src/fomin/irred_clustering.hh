#ifndef __IRRED_CLUSTERING__78104594
#define __IRRED_CLUSTERING__78104594

#include "aux.hh"
#include "common/algorithm.hh"
#include "util/constraints.hh"

namespace kumar {
  size_t div(size_t p, size_t q)
  {
      assert(q != 0);
      return (p + q - 1) / q;
  }

  template <class FwdIt>
  auto kcompute_cost(const cc::vector& pts, FwdIt first, FwdIt last, const cc::vector& clusters)
  {
      // auto dist_sq = cost::kmeans {};

      // unit_type cost {};
      // for (auto it = first; it != last; ++it) {
      //     const auto& pt = pts[it->first];
      //     center c;
      //     for (size_t i {}; i < clusters.size(); ++i) {
      //         const auto& ct = clusters[i];
      //         c.try_assign(i, dist_sq(ct, pt));
      //     }
      //     cost += c.cost();
      // }

      cc::unit cost {};
      for (auto it = first; it != last; ++it) {
          cost += it->second;
      }
      return cost;
  }

  template <class Gen, class SamplingProc>
  auto irred_clustering(aux& aux, Gen& gen, SamplingProc& sampling_proc, const cc::vector& cts, const uti::constraints& r, size_t num_trials, size_t offset = 0, typename cc::vector::unit_type sum = {}) -> typename aux::result_type
  {
      using unit = typename cc::vector::unit_type;
      using result_type = util::result_cost<cc::vector, unit>;
      auto recursion_level = cts.size();

      // If we've found k centres, then assign the remaning points to their closest centres.
      if (cts.size() == aux.num_clusters()) {
          auto& auxspace = aux.auxspace(recursion_level - 1);

          for (auto it = auxspace.begin() + offset; it != auxspace.end(); ++it) {
              sum += it->second;
          }

          return { cts, sum };
      }

      if (offset >= aux.points().size()) {
          return { cts, sum };
      }

      auto& auxspace = aux.auxspace(recursion_level);
      if (recursion_level == 0) {
          for (size_t i {}; i < aux.points().size(); ++i) {
              auxspace[i].first = i;
          }
      } else {
          const auto& prev = aux.auxspace(recursion_level - 1);
          std::copy(prev.begin(), prev.end(), auxspace.begin());
      }

      auto ct = sampling_proc(aux, gen, recursion_level, offset);
      result_type res;
      for (size_t index {}; index < r.width(); ++index) {
          auto ct2 = r.best_fit(index, ct);
          auto r2 = r.restrict(index, ct2);

          auto next_cts = cts;
          next_cts.push_back(ct2);

          auto first = auxspace.begin() + offset;
          auto last = auxspace.end();

          /* Update distances */
          for (auto it = first; it != last; ++it) {
              const auto& pt = aux.points()[it->first];
              bool cost_set {};
              unit cost {};
              for (const auto& ct : next_cts) {
                  auto cost2 = cost::hamming {}(pt, ct);

                  if (!cost_set || cost2 < cost) {
                      cost = cost2;
                      cost_set = true;
                  }
              }
              it->second = cost;
          }

          auto elements_left = std::distance(first, last);
          auto off = offset;
          while (elements_left > 0) {
              auto give = div(elements_left, 2);

              std::nth_element(first, first + give, last,
                               [](const auto& a, const auto& b)
                               { return a.second < b.second; });
              sum += kcompute_cost(aux.points(), first, first + give, next_cts);
              off += give;
              first += give;
              elements_left -= give;

              for (size_t t {}; t < num_trials; ++t) {
                  auto next_res = irred_clustering<Gen, SamplingProc>(aux, gen, sampling_proc, next_cts, r2, num_trials, off, sum);
                  res.try_assign(next_res);
              }
          }
      }

      return res;
  }
}

#endif //__IRRED_CLUSTERING__78104594
