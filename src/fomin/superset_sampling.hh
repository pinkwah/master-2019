#ifndef __SUPERSET_SAMPLING__36630253
#define __SUPERSET_SAMPLING__36630253

#include "aux.hh"

namespace kumar {
  template <class Gen>
  auto bv_superset_sampling(aux& aux, Gen& gen, size_t rec, size_t offset) -> typename cc::vector::value_type
  {
      using vector = cc::vector;
      using vec = typename vector::value_type;
      using unit = typename vector::unit_type;

      const auto& auxspace = aux.auxspace(rec);
      vec ct { aux.points().common_data() };
      auto siz = std::make_unique<size_t[]>(ct.dim());

      struct reduce_iter {
          std::unique_ptr<size_t[]>& value;
          const vector& pts;

          struct reduce_ref {
              std::unique_ptr<size_t[]>& ref;
              const vector& pts;

              reduce_ref& operator=(const std::pair<size_t, unit>& x)
              {
                  const auto& pt = pts[x.first];
                  for (size_t i {}; i < pt.dim(); ++i) {
                      ref[i] += pt[i];
                  }
                  return *this;
              }
          };

          reduce_iter& operator++() { return *this; }
          reduce_iter operator++(int) { return *this; }

          reduce_ref operator*()
          { return { value, pts }; }

          using value_type [[maybe_unused]]        = unit;
          using difference_type [[maybe_unused]]   = std::ptrdiff_t;
          using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
          using reference [[maybe_unused]]         = reduce_ref;
          using pointer [[maybe_unused]]           = reduce_ref*;
      } reduce_iter1 { siz, aux.points() };

      auto size = static_cast<size_t>(std::ceil(aux.num_clusters() * 2.0 / aux.epsilon()));
      size = std::min(size, static_cast<size_t>(std::distance(auxspace.begin() + offset, auxspace.end())));

      std::sample(auxspace.begin() + offset, auxspace.end(), reduce_iter1, size, gen);

      for (size_t i {}; i < ct.dim(); i++) {
          ct[i] = (2*siz[i] >= size) ? 1 : 0;
      }

      return ct;
  }
}


#endif //__SUPERSET_SAMPLING__36630253
