#include <iostream>
#include <random>
#include <fstream>
#include <getopt.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <real.hh>
#include "options.hh"

using fmt::print;

int generate(options& opts)
{
    using point = euclidean::vec<double>;
    using vector = vec_vector<point>;

    print("Choosing {} centers.\n", opts.num_clusters);

    std::mt19937 gen { std::random_device {}() };
    std::uniform_real_distribution<> point_rand(-1, 1);
    std::uniform_real_distribution<> center_radius(0.001, 0.1);

    auto common = euclidean::common<0>(opts.num_dim);

    vector pset(common);
    for (size_t c {}; c < opts.num_clusters; ++c) {
        point center(common);
        std::vector<std::normal_distribution<>> dist_rands;

        for (size_t i {}; i < opts.num_dim; ++i) {
            center[i] = point_rand(gen);
            dist_rands.emplace_back(center[i], center_radius(gen));
        }

        for (size_t i {}; i < static_cast<size_t>(opts.ratio * opts.num_pts) / opts.num_clusters; ++i) {
            point p(common);

            for (size_t j {}; j < opts.num_dim; ++j)
                p[j] = dist_rands[j](gen);

            pset.push_back(p);
        }
    }

    for (size_t i {}; i < static_cast<size_t>((1.0 - opts.ratio) * opts.num_pts); ++i) {
        point p(common);
        for (auto& x : p) {
            x = point_rand(gen);
        }
        pset.push_back(p);
    }

    print("Writing to '{}'\n", opts.output_file);
    std::ofstream outf(opts.output_file);
    print(outf, "{} {} {}\n", opts.num_dim, 0, pset.size());

    fmt::print("Writing {} points to '{}'\n", pset.size(), opts.output_file);
    for (const auto& p : pset) {
        for (auto x : p) {
            outf << x << " ";
        }
        outf << std::endl;
    }

    return 0;
}
