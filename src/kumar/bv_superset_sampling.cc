#include <kumar.hh>
#include <common/options.hh>

namespace {
  class s_major_reduce {
  public:
      class reference {
          std::vector<size_t>& m_ham;
          size_t& m_siz;
          const cc::set& m_pts;

      public:
          reference(std::vector<size_t>& ham, size_t& siz, const cc::set& pts):
              m_ham(ham),
              m_siz(siz),
              m_pts(pts)
          {}

          reference& operator=(const std::pair<size_t, size_t>& x)
          {
              const auto& pt = m_pts[x.first];
              for (size_t i {}; i < pt.dim(); ++i) {
                  m_ham[i] += pt[i];
              }
              ++m_siz;
              return *this;
          }
      };

      using value_type [[maybe_unused]]        = cc::unit;
      using difference_type [[maybe_unused]]   = std::ptrdiff_t;
      using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
      using pointer [[maybe_unused]]           = void; // We don't care

  private:
      std::vector<size_t> m_ham;
      size_t m_siz;
      const cc::set& m_pts;

  public:
      s_major_reduce(const cc::set& pts):
          m_ham(pts.dim()),
          m_siz(0),
          m_pts(pts)
      {}

      s_major_reduce& operator++()
      { return *this; }

      s_major_reduce operator++(int)
      { return *this; }

      reference operator*()
      { return { m_ham, m_siz, m_pts }; }

      cc::vec value()
      {
          cc::vec out(m_pts.dim());
          for (size_t i {}; i < m_pts.dim(); ++i) {
              out[i] = (2*m_ham[i] >= m_siz) ? 1 : 0;
          }
          return out;
      }
  };
}

cc::vec kumar::superset_sampling(aux& aux, size_t rec, size_t offset)
{
    const auto& auxspace = aux.auxspace();
    s_major_reduce reducer { aux.points() };

    auto size = static_cast<size_t>(std::ceil(aux.num_clusters() * 2.0 / aux.epsilon()));
    size = std::min(size, static_cast<size_t>(std::distance(auxspace.begin() + offset, auxspace.end())));

    std::sample(auxspace.begin() + offset, auxspace.end(), reducer, size, common::g_gen);

    return reducer.value();
}
