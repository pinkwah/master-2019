#include <kumar.hh>
#include "superset_sampling.hh"

namespace {
  template <class FwdIt>
  auto kcompute_cost(const cc::set& pts, FwdIt first, FwdIt last, const cc::set& clusters)
  {
      // auto dist_sq = cost::kmeans {};

      // unit_type cost {};
      // for (auto it = first; it != last; ++it) {
      //     const auto& pt = pts[it->first];
      //     center c;
      //     for (size_t i {}; i < clusters.size(); ++i) {
      //         const auto& ct = clusters[i];
      //         c.try_assign(i, dist_sq(ct, pt));
      //     }
      //     cost += c.cost();
      // }

      cc::unit cost {};
      for (auto it = first; it != last; ++it) {
          cost += it->second;
      }
      return cost;
  }
}

cc::result kumar::irred_clustering(aux& aux, const cc::set& cts, size_t num_trials, size_t offset, cc::unit sum)
{
    /* If we've found k centres, then assign the remaning points to their
     * closest centres. */
    if (cts.size() == aux.num_clusters() || offset >= aux.points().size()) {
        return { cts, common::compute_cost(aux.points(), cts) };
    }

    auto& auxspace = aux.auxspace();

    auto ct = superset_sampling(aux, cts.size(), offset);
    cc::result res;
    auto next_cts = cts;
    next_cts.push_back(ct);

    auto first = auxspace.begin() + offset;
    auto last = auxspace.end();

    /* Update distances */
    for (auto it = first; it != last; ++it) {
        const auto& pt = aux.points()[it->first];
        auto new_cost = pt.dist(ct);
        if (new_cost < it->second) {
            it->second = new_cost;
        }
    }

    /* */
    auto num_unassigned = std::distance(first, last);
    while (num_unassigned > 0) {
        auto give = uti::divceil(num_unassigned, 2);

        std::nth_element(first, first + give, last,
                         [](const auto& a, const auto& b)
                         { return a.second < b.second; });
        offset += give;
        first += give;
        num_unassigned -= give;

        for (size_t t {}; t < num_trials; ++t) {
            auto next_res = irred_clustering(aux, next_cts, num_trials, offset, sum);
            res.try_assign(next_res);
        }
    }

    return res;
}
