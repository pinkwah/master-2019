#ifndef __IRRED_CLUSTERING__78104594
#define __IRRED_CLUSTERING__78104594

#include "aux.hh"
#include "common/algorithm.hh"

namespace kumar {
  cc::result irred_clustering(aux& aux, const cc::set& cts, size_t num_trials, size_t offset = 0, cc::unit sum = 0);
}

#endif //__IRRED_CLUSTERING__78104594
