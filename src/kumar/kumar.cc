#include <string_view>
#include <string>

#include "common/start.hh"
#include "common/cost.hh"
#include "common/post_process.hh"
#include "lloyd.hh"
#include "kumar.hh"

using namespace std::string_view_literals;

namespace {
  size_t s_iterations = 1;
  size_t s_lloyd = 0;
  bool s_do_trace {};
  double s_skip = 0;
  double s_epsilon = 0.5;
  std::vector<std::vector<std::vector<size_t>>> s_trace;

  //
  // Kumar
  //
  class s_kumar : public common::algorithm<s_kumar> {
  public:
      cc::result trial(const cc::set& points) override {
          using gen = std::decay_t<decltype(common::g_gen)>;

          kumar::aux aux(points, common::g_options.num_clusters, s_epsilon, s_skip);
          lloyd::aux l_aux(common::g_options.num_clusters, points);
          cc::set empty(points.dim());
          cc::result res;

          auto& trace = s_trace[common::g_thread_id].emplace_back();

          for (size_t i {}; i < s_iterations; ++i) {
#if NORMAL
              auto k_res = kumar::irred_clustering(aux, empty, s_iterations, 0);
#elif BINARY_VECTOR
              auto k_res = kumar::irred_clustering(aux, empty, s_iterations, 0);
#else
#error Invalid
#endif

              if (s_do_trace) {
                  auto kopy_res = k_res;
                  common::post_process(aux.points(), kopy_res);
                  trace.push_back(kopy_res.cost());
              }

              if (s_lloyd > 0) {
                  l_aux.curr_clusters() = k_res.get();

                  /* Fill up on centres */
                  std::sample(
                      points.begin(),
                      points.end(),
                      std::back_inserter(l_aux.curr_clusters()),
                      l_aux.num_clusters() - l_aux.curr_clusters().size(),
                      common::g_gen);

                  for (size_t j {}; j < s_lloyd; ++j) {
#if NORMAL
                      lloyd::perform(l_aux);
#elif BINARY_VECTOR
                      lloyd::bv_perform(l_aux);
#else
#error Invalid
#endif
                  }

                  res.try_assign(l_aux.result());

                  if (s_do_trace) {
                      cc::result copy_res = l_aux.result();
                      common::post_process(aux.points(), copy_res);
                      trace.push_back(copy_res.cost());
                  }
              } else {
                  res.try_assign(k_res);
              }
          }

          return res;
      };

      //
      //
      //
      void post_program(const yaml::dict& y) const override
      {
          y.value("iterations", s_iterations);

          if (s_do_trace) {
              auto t = y.subarray("trace");
              for (const auto& thread : s_trace) {
                  for (const auto& trial : thread) {
                      t.inline_array(trial.begin(), trial.end());
                  }
              }
          }
      }

      std::string_view name() const override {
          return
#if NORMAL
          "kumar"sv;
#elif BINARY_VECTOR
          "bv_kumar"sv;
#endif
      }
  };
}

//
// main
//
int main(int argc, char **argv)
{
    namespace po = boost::program_options;

    po::options_description desc("Lloyd options");
    desc.add_options()
        ("iterations,i", po::value(&s_iterations), "Number of iterations per trial")
        ("lloyd,l", po::value(&s_lloyd), "Number of iterations of Lloyd's to run after")
        ("epsilon,e", po::value(&s_epsilon), "Epsilon")
        ("trace", po::bool_switch(&s_do_trace), "Trace the cost")
        ("skip", po::value(&s_skip), "Probability of skipping a Kumar iteration");

    common::setup(argc, argv, desc);

    s_trace.resize(common::g_options.num_threads);

    common::start(s_kumar {});
}
