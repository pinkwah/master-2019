#ifndef __SUPERSET_SAMPLING__36630253
#define __SUPERSET_SAMPLING__36630253

#include "aux.hh"

namespace kumar {
  cc::vec superset_sampling(aux& aux, size_t rec, size_t offset);
}


#endif //__SUPERSET_SAMPLING__36630253
