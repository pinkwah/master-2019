#ifndef __AUX__20442552
#define __AUX__20442552

#include <vector>
#include "config.hh"
#include <common/algorithm/compute_cost.hh>
#include "util/result.hh"

namespace lloyd {
  class aux {
  public:
      using vector_type = cc::set;
      using unit_type = cc::unit;
      using result_type = cc::result;

      explicit aux(size_t num_clusters, const vector_type& points):
          m_points(points),
          m_num_clusters(num_clusters),
          m_curr_clusters(points.dim(), num_clusters),
          m_next_clusters(points.dim(), num_clusters),
          m_sizes(num_clusters)
      {}

      size_t dim() const
      { return m_points.dim(); }

      size_t num_clusters() const
      { return m_num_clusters; }

      const vector_type& points() const
      { return m_points; }

      vector_type& curr_clusters()
      { return m_curr_clusters; }

      vector_type& next_clusters()
      { return m_next_clusters; }

      std::vector<size_t>& sizes()
      { return m_sizes; }

      bool converged() const
      { return m_converged; }

      size_t num_iter() const
      { return m_num_iter; }

      /**
       * Performs cleanup
       */
      void end_iter()
      {
          ++m_num_iter;
          m_converged = m_next_clusters == m_curr_clusters;
          std::swap(m_next_clusters, m_curr_clusters);
          std::fill(m_sizes.begin(), m_sizes.end(), 0);
      }

      cc::result result() const
      { return { m_curr_clusters, common::compute_cost(m_points, m_curr_clusters) }; }

  private:
      size_t m_num_iter {};

      const vector_type& m_points;
      size_t m_num_clusters;
      vector_type m_curr_clusters;
      vector_type m_next_clusters;

      std::vector<size_t> m_sizes;

      bool m_converged {};
  };
}

#endif //__AUX__20442552
