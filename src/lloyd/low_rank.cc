#include "config.hh"
#include "low_rank.hh"
#include "perform.hh"
#include <util/constraints.hh>
#include <uti/min.hh>

namespace {
  void s_set_row(lloyd::aux& aux, size_t index, size_t row)
  {
      for (size_t i {}; i < aux.num_clusters(); ++i) {
          auto cen = aux.next_clusters()[i];
          cen[index] = bin::unit { (row >> i) & 1 };
      }
  }
}

//
// lloyd::lr_perform
//
void lloyd::lr_perform(lloyd::aux& aux, const std::vector<size_t>& cons)
{
    using closest_type = util::result_cost<size_t, size_t>;

    struct cluster {
        size_t index {};
        size_t size {};
        size_t cost {};
        std::vector<size_t> hamming_weight;

        cluster(const lloyd::aux& aux):
            hamming_weight(aux.dim(), 0_sz)
        {}
    };

    std::vector<cluster> clusters { aux.num_clusters(), cluster { aux } };

    /* Set indices */
    for (size_t i {}; i < clusters.size(); ++i) {
        clusters[i].index = i;
    }

    /* Count the sizes of partitions */
    for (const auto& pt : aux.points()) {
        /* Find the the point's cluster O(dk) */
        closest_type closest {};
        for (size_t j {}; j < aux.num_clusters(); ++j) {
            const auto& c = aux.curr_clusters()[j];
            closest.try_assign(j, cost::hamming {}(pt, c));
        }

        /* Update it */
        auto& cluster = clusters[closest.get()];

        ++cluster.size;
        for (size_t i {}; i < aux.dim(); ++i) {
            if (pt[i] != 0) {
                ++cluster.hamming_weight[i];
                ++cluster.cost;
            }
        }
    }

    auto ccost = [&](size_t index, size_t row) {
        size_t f {};
        /* Compute the cost */
        for (size_t i {}; i < aux.num_clusters(); ++i) {
            const auto& cluster = clusters[i];
            if (!((row >> i) & 1)) {
                f += cluster.hamming_weight[index];
            } else {
                f += (cluster.size - cluster.hamming_weight[index]);
            }
        }
        return f;
    };

    /* Select next */
    for (size_t i {}; i < aux.dim(); ++i) {
        util::result_cost<size_t, size_t> best_row;

        for (const auto& row : cons) {
            best_row.try_assign(row, ccost(i, row));
        }

        s_set_row(aux, i, best_row.get());
    }

    aux.end_iter();
}
