#ifndef __PERFORM__66600699
#define __PERFORM__66600699

#include "aux.hh"
#include "util/constraints.hh"

namespace lloyd {
  void perform(lloyd::aux& aux);
  void bv_perform(lloyd::aux& aux);
  void lr_perform(lloyd::aux& aux, const std::vector<size_t>& r);
}

#endif //__PERFORM__66600699
