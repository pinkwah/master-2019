#ifndef __RANDOM_SEED__63317226
#define __RANDOM_SEED__63317226

#include <random>
#include <functional>
#include "aux.hh"

namespace lloyd {
  inline void random_seed(aux& aux);
  inline void furthest_seed(aux& aux);
}

//
// random_seed
//
inline void lloyd::random_seed(lloyd::aux& aux)
{
    const auto& pts = aux.points();
    auto& cur = aux.curr_clusters();
    std::sample(pts.begin(), pts.end(), cur.begin(), cur.size(), common::g_gen);
}

//
// furthest_seed
//
inline void lloyd::furthest_seed(aux& aux)
{
    std::uniform_int_distribution uniform { static_cast<size_t>(0), aux.points().size() - 1};

    using vec = cc::vec;
    using unit = cc::unit;

    const auto& pts = aux.points();
    auto& cts = aux.curr_clusters();
    cts[0] = pts[uniform(common::g_gen)];

    std::vector<cc::unit> probabilities {};
    probabilities.resize(aux.points().size());
    for (size_t i {}; i < aux.points().size(); ++i) {
        probabilities[i] = cts[0].dist(pts[i]);
    }

    for (size_t i = 1; i < cts.size(); ++i) {
        std::discrete_distribution distribution { probabilities.begin(), probabilities.end() };

        cts[i] = pts[distribution(common::g_gen)];
        for (size_t j {}; j < aux.points().size(); ++j) {
            auto c = cts[i].dist(pts[j]);
            if (c < probabilities[j]) {
                probabilities[j] = c;
            }
        }
    }
}

#endif //__RANDOM_SEED__63317226
