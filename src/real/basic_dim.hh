#ifndef __BASIC_DIM__28823385
#define __BASIC_DIM__28823385

#include <stdexcept>
#include <cstddef>
#include <fmt/core.h>

namespace real {
  constexpr size_t Dynamic = 0;

  template <size_t N>
  class basic_dim {
  public:
      basic_dim() = delete;

      constexpr basic_dim(size_t n)
      {
          if (n <= N)
              return;
          fmt::print(stderr, "Invalid basic_dim size\n");
          std::fflush(stderr);
          std::abort();
      }

      template <size_t M>
      constexpr basic_dim(const basic_dim<M>& other)
      {
          if (static_cast<size_t>(other) <= N)
              return;
          static_assert(N <= M, "Static basic_dimensino size");
      }

      constexpr basic_dim& operator=(const basic_dim&) = default;

      constexpr operator size_t() const
      { return N; }
  };

  template <>
  class basic_dim<Dynamic> {
      size_t m_basic_dim;

  public:
      basic_dim() = delete;

      constexpr basic_dim(size_t n):
          m_basic_dim(n)
      {}

      template <size_t N>
      constexpr basic_dim(const basic_dim<N>& other):
          m_basic_dim(static_cast<size_t>(other))
      {}

      constexpr basic_dim& operator=(const basic_dim&) = default;

      constexpr operator size_t() const
      { return m_basic_dim; }
  };
}

#endif //__BASIC_DIM__28823385
