#ifndef __BASIC_VEC__87555727
#define __BASIC_VEC__87555727

#include <boost/compressed_pair.hpp>
#include <boost/functional/hash.hpp>
#include <memory>
#include <initializer_list>
#include "mixin.hh"
#include "traits.hh"

namespace real {
  template <class T, size_t N>
  struct mixin_types<basic_vec<T, N>> {
      using traits_type            = traits<T, N>;
      using dim_type               = basic_dim<N>;
      using value_type             = T;
      using pointer                = T*;
      using const_pointer          = const T*;
      using reference              = T&;
      using const_reference        = const T&;
      using iterator               = T*;
      using const_iterator         = const T*;
      using reverse_iterator       = std::reverse_iterator<iterator>;
      using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  };

/**
 * euclidean::basic_vec container
 */
  template <class T, size_t N = 0>
  class basic_vec : public mixin<basic_vec<T, N>, T, N> {
  public:
      using traits_type            = typename mixin_types<basic_vec>::traits_type;
      using dim_type               = typename mixin_types<basic_vec>::dim_type;
      using value_type             = typename mixin_types<basic_vec>::value_type;
      using pointer                = typename mixin_types<basic_vec>::pointer;
      using const_pointer          = typename mixin_types<basic_vec>::const_pointer;
      using reference              = typename mixin_types<basic_vec>::reference;
      using const_reference        = typename mixin_types<basic_vec>::const_reference;
      using iterator               = typename mixin_types<basic_vec>::iterator;
      using const_iterator         = typename mixin_types<basic_vec>::const_iterator;
      using reverse_iterator       = typename mixin_types<basic_vec>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<basic_vec>::const_reverse_iterator;

  private:
      template <class, class, size_t>
      friend class mixin;

      template <class, size_t>
      friend class basic_ref;

      alignas(uti::packet_traits<T>::align) std::array<T, N> m_data;

  protected:
      /**
       * Data access
       */
      pointer p_data()
      { return m_data.data(); }

      const_pointer p_data() const
      { return m_data.data(); }

      dim_type p_dim() const
      { return dim_type { N }; }

  public:
      basic_vec() = delete;

      basic_vec(const basic_vec& other)
      { std::copy(other.begin(), other.end(), this->begin()); }

      basic_vec(basic_vec&& other) = default;

      basic_vec(const dim_type&)
      { std::fill(this->begin(), this->end(), 0); }

      template <class U, class = std::enable_if_t<is_real_v<U>>>
      basic_vec(const U& other)
      { std::copy(other.begin(), other.end(), this->begin()); }

      basic_vec(std::initializer_list<T> il)
      { std::copy(il.begin(), il.end(), this->begin()); }

      /**
       * Assignment operator
       */
      basic_vec& operator=(const basic_vec& other)
      {
          std::copy(other.begin(), other.end(), this->begin());
          return *this;
      }

      basic_vec& operator=(basic_vec&&) = default;
  };

/**
 * euclidean::basic_vec dynamic container
 */
  template <class T>
  class basic_vec<T, 0> : public mixin<basic_vec<T, 0>, T, 0> {
  public:
      using traits_type            = traits<T, 0>;
      using dim_type               = typename traits_type::dim_type;
      using value_type             = T;
      using pointer                = T*;
      using const_pointer          = const T*;
      using reference              = T&;
      using const_reference        = const T&;
      using iterator               = T*;
      using const_iterator         = const T*;
      using reverse_iterator       = std::reverse_iterator<iterator>;
      using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  private:
      template <class, class, size_t>
      friend class mixin;

      uti::unique_ptr<T> m_data;
      dim_type m_dim;

  protected:
      /**
       * Data access
       */
      pointer p_data()
      { return m_data.get(); }

      const_pointer p_data() const
      { return m_data.get(); }

      dim_type p_dim() const
      { return m_dim; }

  public:
      basic_vec() = delete;

      basic_vec(const basic_vec& other):
          m_data(uti::make_unique<T>(static_cast<size_t>(other.dim()))),
          m_dim(other.m_dim)
      { std::copy(other.begin(), other.end(), this->begin()); }

      basic_vec(basic_vec&& other) = default;

      basic_vec(dim_type dim):
          m_data(uti::make_unique<T>(dim)),
          m_dim(dim)
      { std::fill(this->begin(), this->end(), 0); }

      template <class U, class = std::enable_if_t<is_real_v<U>>>
      basic_vec(const U& other):
          m_data(uti::make_unique<T>(static_cast<size_t>(other.dim()))),
          m_dim(other.dim())
      {
          std::copy(other.begin(), other.end(), this->begin());
      }

      basic_vec(std::initializer_list<T> il):
          m_data(uti::make_unique<T>(il.size())),
          m_dim(il.size())
      {
          std::copy(il.begin(), il.end(), this->begin());
      }

      /**
       * Assignment operator
       */
      basic_vec& operator=(const basic_vec& other)
      {
          if (other.dim() > this->dim()) {
              m_data = uti::make_unique<T>(other.dim());
          }
          m_dim = other.dim();
          std::copy(other.begin(), other.end(), this->begin());
          return *this;
      }

      basic_vec& operator=(basic_vec&&) = default;
  };
}

template <class T, size_t N>
struct std::hash<real::basic_vec<T, N>> {
    size_t operator()(const real::basic_vec<T, N>& vec) const {
        return boost::hash_range(vec.begin(), vec.end());
    }
};

#endif //__BASIC_VEC__87555727
