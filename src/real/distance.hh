#ifndef __DISTANCE__70098690
#define __DISTANCE__70098690

#include <algorithm>
#include <numeric>
#include <functional>

template <class A, class B>
auto dist_sq(const A& a, const B& b) -> typename A::value_type
{
    static_assert(std::is_same_v<typename A::value_type, typename B::value_type>);
    assert(a.dim() == b.dim());
    using T = typename A::value_type;
    T sum {};
    for (size_t i {}; i < a.dim(); ++i) {
        auto c = a[i] - b[i];
        sum += c*c;
    }
    return sum;
}

#endif //__DISTANCE__70098690
