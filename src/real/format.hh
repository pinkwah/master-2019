#ifndef __FORMAT__69917014
#define __FORMAT__69917014

#include <fmt/core.h>

/**
 * Formatting routine
 */
template <class T, size_t N>
struct fmt::formatter<::real::basic_ref<T, N>> {
    using ref = ::real::basic_ref<T, N>;

    template <class ParseContext>
        constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
        auto format(const ref& v, FormatContext& ctx)
    {
        auto it = format_to(ctx.begin(), "{{ ");
        for (size_t i {}; i + 1 < v.dim(); ++i) {
            it = format_to(it, "{}, ", v[i]);
        }
        return format_to(it, "{} }}", v.back());
    }
};

/**
 * Formatting routine
 */
template <class T, size_t N>
struct fmt::formatter<::real::basic_vec<T, N>> {
    using vec = ::real::basic_vec<T, N>;

    template <class ParseContext>
        constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
        auto format(const vec& v, FormatContext& ctx)
    {
        auto it = format_to(ctx.begin(), "{{ ");
        for (size_t i {}; i + 1 < v.dim(); ++i) {
            it = format_to(it, "{}, ", v[i]);
        }
        return format_to(it, "{} }}", v.back());
    }
};

/**
 * Formatting routine
 */
template <class T, size_t N>
struct fmt::formatter<::real::basic_view<T, N>> {
    using basic_view = ::real::basic_view<T, N>;

    template <class ParseContext>
        constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
        auto format(const basic_view& v, FormatContext& ctx)
    {
        auto it = format_to(ctx.begin(), "{{ ");
        for (size_t i {}; i + 1 < v.dim(); ++i) {
            it = format_to(it, "{}, ", v[i]);
        }
        return format_to(it, "{} }}", v.back());
    }
};

/**
 * Formatting routine
 */
template <class T, size_t N>
struct fmt::formatter<::real::basic_set<T, N>> {
    using obj = ::real::basic_set<T, N>;

    template <class ParseContext>
    constexpr auto parse(ParseContext& ctx)
    { return ctx.begin(); }

    template <class FormatContext>
    auto format(const obj& v, FormatContext& ctx)
    {
        auto it = format_to(ctx.begin(), "{{ ");
        for (size_t i {}; i + 1 < v.size(); ++i) {
            it = format_to(it, "{}, ", v[i]);
        }
        return format_to(it, "{} }}", v.back());
    }
};

#endif //__FORMAT__69917014
