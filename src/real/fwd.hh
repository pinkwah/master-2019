#ifndef __FWD__12967841
#define __FWD__12967841

#include <cstddef>

namespace real {
  template <class T, size_t N> class basic_vec;
  template <class T, size_t N> class basic_ref;
  template <class T, size_t N> class basic_view;
  template <class T, size_t N> class basic_set;
}

#endif //__FWD__12967841
