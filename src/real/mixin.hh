#ifndef __MIXIN__56065111
#define __MIXIN__56065111

#include <algorithm>
#include <numeric>
#include <functional>

#include "uti/packet.hh"
#include "fwd.hh"

namespace real {
  template <class T>
  struct mixin_types {};

  template <class Self, class T, size_t N>
  class mixin : public mixin_types<Self> {
  public:
      using traits_type            = typename mixin_types<Self>::traits_type;
      using dim_type               = typename mixin_types<Self>::dim_type;
      using value_type             = typename mixin_types<Self>::value_type;
      using pointer                = typename mixin_types<Self>::pointer;
      using const_pointer          = typename mixin_types<Self>::const_pointer;
      using reference              = typename mixin_types<Self>::reference;
      using const_reference        = typename mixin_types<Self>::const_reference;
      using iterator               = typename mixin_types<Self>::iterator;
      using const_iterator         = typename mixin_types<Self>::const_iterator;
      using reverse_iterator       = typename mixin_types<Self>::reverse_iterator;
      using const_reverse_iterator = typename mixin_types<Self>::const_reverse_iterator;
      using packet_type            = uti::packet<T>;

  private:
      friend class basic_vec<T, N>;
      friend class basic_ref<T, N>;
      template <class, class, size_t>
      friend class mixin;

      using self = Self;
      using owning = basic_vec<T, N>;

      self& m_self()
      { return *static_cast<self*>(this); }

      const self& m_self() const
      { return *static_cast<const self*>(this); }

      packet_type* m_packet_begin()
      { return reinterpret_cast<packet_type*>(data()); }

      packet_type* m_packet_end()
      { return reinterpret_cast<packet_type*>(data() + uti::round_up(size(), uti::packet_traits<T>::count)); }

      const packet_type* m_packet_begin() const
      { return reinterpret_cast<const packet_type*>(data()); }

      const packet_type* m_packet_end() const
      { return reinterpret_cast<const packet_type*>(data() + uti::round_up(size(), uti::packet_traits<T>::count)); }

      template <class U, class Func>
      self& m_transform(const U& vec, Func&& func)
      {
          std::transform(
              m_packet_begin(),
              m_packet_end(),
              vec.m_packet_begin(),
              m_packet_begin(),
              std::forward<Func>(func));
          return m_self();
      }

      template <class Func>
      self& m_transform(Func&& func)
      {
          std::transform(m_packet_begin(), m_packet_end(), m_packet_begin(), std::forward<Func>(func));
          return m_self();
      }

      template <class U, class Func>
      owning m_transform_copy(const U& vec, Func&& func) const
      {
          owning cpy = m_self();
          cpy.m_transform(vec, std::forward<Func>(func));
          return cpy;
      }

      template <class Func>
      owning m_transform_copy(Func&& func) const
      {
          owning cpy = m_self();
          cpy.m_transform(std::forward<Func>(func));
          return cpy;
      }

  public:
      /**
       * Vector Boolean Operators
       */
      template <class U>
      bool operator==(const U& vec) const
      {
          for (size_t i {}; i < dim(); ++i) {
              if (std::abs((*this)[i] - vec[i]) > std::numeric_limits<T>::epsilon())
                  return false;
          }
          return true;
      }

      template <class U>
      bool operator!=(const U& vec) const
      {
          return std::inner_product(
              vec.begin(),
              vec.end(),
              begin(),
              true,
              std::logical_or<bool> {},
              std::not_equal_to<T> {});
      }

      /**
       * Vector Arithmetic Operators
       */
      template <class U>
      self& operator+=(const U& vec)
      { return m_transform(vec, std::plus<packet_type> {}); }

      template <class U>
      self& operator-=(const U& vec)
      { return m_transform(vec, std::minus<packet_type> {}); }

      self& operator/=(const value_type& s)
      { return m_transform([&s](const packet_type& x) { return x / s; }); }

      self& operator*=(const value_type& s)
      { return m_transform([&s](const packet_type& x) { return x * s; }); }

      template <class U>
      owning operator+(const U& vec) const
      { return m_transform_copy(vec, std::plus<packet_type> {}); }

      template <class U>
      owning operator-(const U& vec) const
      { return m_transform_copy(vec, std::minus<packet_type> {}); }

      owning operator/(const value_type& s) const
      { return m_transform_copy([&s](const packet_type& x) { return x / s; }); }

      owning operator*(const value_type& s) const
      { return m_transform_copy([&s](const packet_type& x) { return x * s; }); }

      owning operator-() const
      { return m_transform_copy([](const packet_type& x) { return -x; }); }

      owning operator~() const
      { return m_transform_copy([](const packet_type& x) { return !x; }); }

//      T norm() const
//      { return std::inner_product(begin(), end(), begin()); }

      template <class U>
      T dist(const U& other) const
      {
          uti::packet<T> val;
          auto it1 = m_packet_begin();
          auto it2 = other.m_packet_begin();
          auto end = m_packet_end();

          for (; it1 != end; ++it1, ++it2) {
              auto x = *it1 - *it2;
              val.madd(x, x);
          }

          return val.sum();
      }

      /**
       * Data access
       */
      pointer data()
      { return m_self().p_data(); }

      const_pointer data() const
      { return m_self().p_data(); }

      dim_type dim() const
      { return m_self().p_dim(); }

      size_t size() const
      { return m_self().p_dim(); }

      /**
       * Accessors
       */
      reference at(size_t idx)
      { return begin()[idx]; }

      const_reference at(size_t idx) const
      { return begin()[idx]; }

      reference operator[](size_t idx)
      { return begin()[idx]; }

      const_reference operator[](size_t idx) const
      { return begin()[idx]; }

      reference front()
      { return (*this)[0]; }

      const_reference front() const
      { return (*this)[0]; }

      reference back()
      { return (*this)[m_self().dim() - 1]; }

      const_reference back() const
      { return (*this)[m_self().dim() - 1]; }

      /**
       * Iterators
       */
      iterator begin()
      { return data(); }

      const_iterator begin() const
      { return data(); }

      const_iterator cbegin() const
      { return data(); }

      iterator end()
      { return data() + dim(); }

      const_iterator end() const
      { return data() + dim(); }

      const_iterator cend() const
      { return data() + dim(); }

      reverse_iterator rbegin()
      { return end(); }

      const_reverse_iterator rbegin() const
      { return end(); }

      const_reverse_iterator crbegin() const
      { return end(); }

      reverse_iterator rend()
      { return begin(); }

      const_reverse_iterator rend() const
      { return begin(); }

      const_reverse_iterator crend() const
      { return begin(); }
  };
}

#endif //__MIXIN__56065111
