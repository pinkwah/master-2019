#ifndef __SIMD__24148071
#define __SIMD__24148071

#include <array>

namespace real {
  template <class T, size_t N>
  class simd_array {
      T m_array[N];

  public:
      simd_array() = default;
      simd_array(const simd_array&) = default;
      simd_array(simd_array&&) = default;

      simd_array& operator=(const simd_array&) = default;
      simd_array& operator=(simd_array&&) = default;

      simd_array operator+(const simd_array& other) const
      { auto x = *this; x += other; return x; }

      simd_array operator-(const simd_array& other) const
      { auto x = *this; x -= other; return x; }

      simd_array operator*(const simd_array& other) const
      { auto x = *this; x *= other; return x; }

      simd_array operator/(const simd_array& other) const
      { auto x = *this; x /= other; return x; }

      simd_array operator%(const simd_array& other) const
      { auto x = *this; x %= other; return x; }

      simd_array operator&(const simd_array& other) const
      { auto x = *this; x &= other; return x; }

      simd_array operator|(const simd_array& other) const
      { auto x = *this; x |= other; return x; }

      simd_array operator^(const simd_array& other) const
      { auto x = *this; x ^= other; return x; }

      bool operator==(const simd_array& other) const
      {
          for (size_t)
      }
  };

  template <class T>
  struct simd {
      using type = T;
      static constexpr size_t skip = 1;
  };

  template <class T, size_t N>
  struct simd<simd_array<T, N>> {
      using type = simd_array<T, N>;
      static constexpr size_t skip = N;
  };

  template <>
  struct simd<double> : simd<simd_array<double, 4>> {};

  template <class T>
  using simd_t = typename simd<T>::type;

  template <class T>
  using simd_skip = typename simd<T>::skip;
}

#endif //__SIMD__24148071
