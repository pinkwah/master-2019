#ifndef __HELPER__52511473
#define __HELPER__52511473

#include <cstddef>

namespace uti {
  constexpr size_t divceil(size_t p, size_t q);
  constexpr size_t round_up(size_t n, size_t q);
}

constexpr size_t uti::divceil(size_t p, size_t q)
{
    return (p + q - 1) / q;
}

constexpr size_t uti::round_up(size_t n, size_t q)
{
    auto r = n % q;
    if (r == 0)
        return n;
    return n + q - r;
}

#endif //__HELPER__52511473
