#ifndef __MIN__68066155
#define __MIN__68066155

#include <type_traits>
#include <utility>
#include <functional>

namespace uti {
  template <class FwdIt, class F, class Less>
  auto min(FwdIt first, FwdIt last, F&& f, Less&& less)
  {
      auto best_cost = f(*first);
      auto it = first;
      for (++it; it != last; ++it) {
          auto cost = f(*it);
          if (less(cost, best_cost)) {
              best_cost = cost;
          }
      }
      return best_cost;
  }

  template <class FwdIt, class F, class Less>
  auto argmin(FwdIt first, FwdIt last, F&& f, Less&& less)
  {
      auto best_iter = first;
      auto best_cost = f(*first);
      auto it = first;
      for (++it; it != last; ++it) {
          auto cost = f(*it);
          if (less(cost, best_cost)) {
              best_iter = it;
              best_cost = cost;
          }
      }
      return best_iter;
  }

  template <class FwdIt, class F>
  auto min(FwdIt first, FwdIt last, F&& f)
  {
      using const_reference = typename FwdIt::const_reference;
      using value_type = std::invoke_result_t<F, const_reference>;
      using less = std::less<value_type>;
      return min(first, last, std::forward<F>(f), less {});
  }

  template <class FwdIt, class F>
  auto argmin(FwdIt first, FwdIt last, F&& f)
  {
      using const_reference = typename FwdIt::const_reference;
      using value_type = std::invoke_result_t<F, const_reference>;
      using less = std::less<value_type>;
      return argmin(first, last, std::forward<F>(f), less {});
  }

  template <class Collection, class F>
  auto min(const Collection& collection, F&& f)
  {
      return min(collection.begin(), collection.end(), std::forward<F>(f));
  }

  template <class Collection, class F>
  auto argmin(const Collection& collection, F&& f)
  {
      return argmin(collection.begin(), collection.end(), std::forward<F>(f));
  }

  template <class Set, class Vec>
  size_t argmin_pt(const Set& set, const Vec& vec)
  {
      auto belem = 0;
      auto bcost = set[0].dist(vec);

      for (size_t i = 1; i < set.size(); ++i) {
          auto cost = set[i].dist(vec);
          if (cost < bcost) {
              belem = i;
              bcost = cost;
          }
      }
      return belem;
  }
}

#endif //__MIN__68066155
