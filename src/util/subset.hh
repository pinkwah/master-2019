#ifndef __SUBSET__31057986
#define __SUBSET__31057986

#include <algorithm>
#include <numeric>
#include <memory>
#include <vector>

namespace uti {
  template <class FwdIt>
  class subset {
      FwdIt m_first;
      FwdIt m_last;
      size_t m_size {};

  public:
      class iterator;
      class reference;

  public:
      subset(FwdIt first, FwdIt last, size_t size):
          m_first(first),
          m_last(last),
          m_size(size)
      {
          if (size == 0) {
              throw std::runtime_error { "subset: size cannot be 0" };
          } else if (size > std::distance(first, last)) {
              throw std::runtime_error { "subset: size cannot exceed N" };
          }
      }

      iterator begin()
      { return { m_first, m_last, m_size }; }

      iterator end()
      { return {}; }
  };

  template <class FwdIt>
  class subset<FwdIt>::iterator {
      bool m_sentinel {};
      FwdIt m_first;
      FwdIt m_last;
      std::vector<FwdIt> m_subset;

      class subset;

  public:
      iterator(FwdIt first, FwdIt last, size_t size):
          m_first(first),
          m_last(last)
      {
          auto it = m_first;
          for (size_t i {}; i < size; ++i) {
              m_subset.emplace_back(it);
              ++it;
          }
      }

      /* sentinel constructor */
      iterator():
          m_sentinel(true)
      {}

      iterator& operator++()
      {
          if (m_subset.size() < 1)
              return *this;

          for (auto first = m_subset.begin(), second = first + 1; second != m_subset.end(); ++first, ++second) {
              if (*first + 1 < *second) {
                  ++*first;
                  std::iota(m_subset.begin(), first, m_first);
                  return *this;
              }
          }

          if (m_subset.back() + 1 != m_last) {
              auto val = m_subset.back();
              std::iota(m_subset.begin(), m_subset.end(), m_first);
              m_subset.back() = val + 1;
          } else {
              m_sentinel = true;
          }

          return *this;
      }

      bool operator!=(const iterator& other) const
      {
          if (m_sentinel != other.m_sentinel) {
              return true;
          } else if (m_sentinel) {
              return false;
          } else {
              return m_subset != other.m_subset;
          }
      }

      subset operator*()
      { return m_subset; }
  };

  template <class FwdIt>
  class subset<FwdIt>::iterator::subset {
      const std::vector<FwdIt> &m_subset;

      class const_iterator;

  public:
      subset(std::vector<FwdIt> &s):
          m_subset(s)
      {}

      auto operator[](size_t i)
      { return *m_subset[i]; }

      const_iterator begin() const
      { return { m_subset.begin() }; }

      const_iterator end() const
      { return m_subset.end(); }
  };

  template <class FwdIt>
  class subset<FwdIt>::iterator::subset::const_iterator {
      using iter_type = typename std::vector<FwdIt>::const_iterator;
      iter_type m_iter;

  public:
      const_iterator(iter_type iter):
          m_iter(iter)
      {}

      bool operator!=(const const_iterator& other) const
      { return m_iter != other.m_iter; }

      const_iterator& operator++()
      { ++m_iter; return *this; }

      typename FwdIt::reference operator*()
      { return **m_iter; }
  };
}

#endif //__SUBSET__31057986
