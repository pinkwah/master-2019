#include <gtest/gtest.h>
#include <util/euclidean.hh>
#include <util/vec_vector.hh>
#include <algo/kumar.hh>
#include <algo/verify.hh>

using vector = vec_vector<euclidean::vec<float>>;

TEST(Kumar, k_1_n_1)
{
    vector pts {
        { 1, 0 }
    };

    algo::kumar_1 kumar(pts, 1, 1.0);
    kumar.run(1);

    ASSERT_EQ(kumar.cost(), 0);
}

TEST(Kumar, k_1_n_2)
{
    vector pts {
        { 15, 20 },
        { 13, -3 }
    };

    algo::kumar_1 kumar(pts, 1, 1.0);
    kumar.run(1);

    auto expect = algo::verify(pts, kumar.result());
    ASSERT_EQ(kumar.result().size(), 1);
    ASSERT_EQ(kumar.cost(), expect);
}

TEST(Kumar, k_2_n_2)
{
    vector pts {
        { 15, 20 },
        { 13, -3 }
    };

    algo::kumar_1 kumar(pts, 2, 1.0);
    kumar.run(1);

    auto expect = algo::verify(pts, kumar.result());
    ASSERT_EQ(kumar.result().size(), 2);
    ASSERT_EQ(kumar.cost(), expect);
}

TEST(Kumar, k_2_n_5)
{
    vector pts {
        { 15, 20 },
        { 13, -3 },
        { -5, 32 },
        { 12, 4 },
        { 65, 2 }
    };

    algo::kumar_1 kumar(pts, 2, 1.0);
    kumar.run(1);

    auto expect = algo::verify(pts, kumar.result());
    ASSERT_EQ(kumar.result().size(), 2);
    ASSERT_EQ(kumar.cost(), expect);
}
