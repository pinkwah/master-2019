#include <gtest/gtest.h>
#include "util/euclidean.hh"
#include "util/vec_vector.hh"
#include "lloyd.hh"
#include "algo/verify.hh"

using vector = vec_vector<euclidean::vec<float>>;

TEST(Lloyd, k_1_single)
{
    vector pts {
        { 1, 0 }
    };

    lloyd::aux aux(1, pts);
    lloyd::perform(aux);
    auto result = aux.move_result();

    ASSERT_EQ(result.cost(), 0);
}

TEST(Lloyd, k_1_five)
{
    vector pts {
        { 15, 20 },
        { 13, -3 }
    };

    lloyd::aux aux(1, pts);
    lloyd::perform(aux);
    auto result = aux.move_result();

    auto expect = algo::verify(pts, result.get());
    ASSERT_EQ(result.get().size(), 1);
    ASSERT_EQ(result.cost(), expect);
}
