#include <gtest/gtest.h>
#include <util/euclidean.hh>
#include <util/vec_vector.hh>

#include <algo/verify.hh>

using vector = vec_vector<euclidean::vec<float>>;

TEST(Verify, single_center)
{
    vector pts {
        { 1, 0 },
        { 0, 0 }
    };

    vector cts {
        { 0, 0 }
    };

    auto actual = algo::verify(pts, cts);
    ASSERT_EQ(actual, 1);
}

TEST(Verify, multi_center)
{
    vector pts {
        { 0, 0 },
        { 1, 0 },
        { 0, 1 },
        { 1, 1 }
    };

    vector cts {
        { 0, 0 },
        { 1, 1 }
    };

    auto actual = algo::verify(pts, cts);
    ASSERT_EQ(actual, 2);
}

// TEST(Verify, multi_center_2)
// {
//     vector pts {
//         { -2, -2 },
//         { 1, 1 },
//         { 2, 1 },
//         { 2, 2 },
//         {  }
//     }
// }
