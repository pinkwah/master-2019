#include <gtest/gtest.h>
#include <bin.hh>

namespace {
  // template <class Set>
  // Set s_make_set()
  // {
  //     using set = set;
  //     using vec = typename set::traits_type::vec_type;
  //     using ref = typename vec::traits_type::ref_type;
  // }
}

using Types = ::testing::Types<
    bin::basic_set<bin::Dynamic>,
    bin::basic_set<1>
    >;


template <class T>
class Set : public ::testing::Test {};

TYPED_TEST_CASE(Set, Types);

TYPED_TEST(Set, push_back)
{
    using set = TypeParam;
    using vec = typename set::traits_type::vec_type;
    using ref = typename vec::traits_type::ref_type;

    set s(4);

    vec v1 { 1, 1, 0, 1 };
    vec v2 { 1, 1, 1, 0 };
    vec v3 { 0, 0, 1, 1 };
    vec v4 { 0, 0, 1, 1 };
    s.push_back(v1);
    s.push_back(v2);
    s.push_back(v3);
    s.push_back(v4);

    ASSERT_EQ(s.size(), 4);
    ASSERT_EQ(s[0], v1);
    ASSERT_EQ(s[1], v2);
    ASSERT_EQ(s[2], v3);
    ASSERT_EQ(s[3], v4);
}
