#include <gtest/gtest.h>
#include <bin/format.hh>
#include <bin/basic_vec.hh>

using Types = ::testing::Types<
    bin::basic_vec<bin::Dynamic>,
    bin::basic_vec<1>
    >;

using namespace std::string_view_literals;

template <class T>
class Basic : public ::testing::Test {};

TYPED_TEST_CASE(Basic, Types);

TYPED_TEST(Basic, equality)
{
    using vec = TypeParam;

    vec a = { 1, 0 };
    const vec b = { 1, 1 };

    /* Reflexivity */
    ASSERT_EQ(a, a);
    ASSERT_EQ(b, b);

    /* Symmetry */
    ASSERT_NE(a, b);
    ASSERT_NE(b, a);
}

TYPED_TEST(Basic, addition_subtraction)
{
    using vec = TypeParam;

    vec a       = { 0, 1, 0, 1 };
    const vec b = { 0, 0, 1, 1 };
    vec expect  = { 0, 1, 1, 0 };

    /* Copy operator */
    EXPECT_EQ(a + b, expect);
    EXPECT_EQ(b + a, expect);
    EXPECT_EQ(a - b, expect);
    EXPECT_EQ(b - a, expect);

    /* Assign operator */
    auto a_plus_b = a, a_minus_b = a;
    a_plus_b += b;
    a_minus_b -= b;

    EXPECT_EQ(a_plus_b, expect);
    EXPECT_EQ(a_minus_b, expect);
}

TYPED_TEST(Basic, scalar_multiplication)
{
    using vec = TypeParam;

    const vec a = { 0, 1, 0, 1 };
    const vec zero = { 0, 0, 0, 0 };

    EXPECT_EQ(a*bin::unit(0), zero);
    EXPECT_EQ(a*bin::unit(1), a);
}

TYPED_TEST(Basic, unary_minus)
{
    using vec = TypeParam;

    const vec a = { 0, 1 };

    EXPECT_EQ(-a, a);
}

TYPED_TEST(Basic, unary_invert)
{
    using vec = TypeParam;

    const vec a = { 0, 1, 1, 0 };
    const vec b = { 1, 0, 0, 1 };

    EXPECT_EQ(~a, b);
}

TYPED_TEST(Basic, norm)
{
    using vec = TypeParam;

    vec a = { 1, 0 };

    ASSERT_EQ(a.norm(), 1);
}

TYPED_TEST(Basic, format)
{
    using vec = TypeParam;

    const vec a = { 0, 1 };
    const auto expect = "{ 0, 1 }"sv;

    auto s = fmt::format("{}", a);
    EXPECT_EQ(s, expect);
}
