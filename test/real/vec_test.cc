#include <gtest/gtest.h>
#include <fmt/format.h>
#include <util/euclidean/vec.hh>
#include <util/euclidean/distance.hh>
#include <util/bignum.hh>

using Types = ::testing::Types<
    euclidean::vec<double, 2>,
    euclidean::vec<int, 2>,
    euclidean::vec<mpz, 2>,
    euclidean::vec<mpq, 2>,
    euclidean::vec<double>,
    euclidean::vec<int>,
    euclidean::vec<mpz>,
    euclidean::vec<mpq>
>;

using namespace std::string_view_literals;

template <class T>
class Basic : public ::testing::Test {};

TYPED_TEST_CASE(Basic, Types);

TYPED_TEST(Basic, equality)
{
    using vec = TypeParam;

    vec a = { 1, 2 };
    const vec b = { 2, 3 };

    /* Reflexivity */
    ASSERT_EQ(a, a);
    ASSERT_EQ(b, b);

    /* Symmetry */
    ASSERT_NE(a, b);
    ASSERT_NE(b, a);
}

TYPED_TEST(Basic, addition)
{
    using vec = TypeParam;

    vec a = { 1, 2 };
    const vec b = { -4, 3 };
    vec expect = { -3, 5 };

    /* Copy operator */
    vec c = a + b;
    EXPECT_EQ(c, expect);

    vec d = b + a;
    EXPECT_EQ(d, expect);

    /* Assign operator */
    a += b;
    EXPECT_EQ(a, expect);
}

TYPED_TEST(Basic, subtraction)
{
    using vec = TypeParam;

    vec a = { 1, 2 };
    const vec b = { -4, 3 };
    vec expect_a_b = { 5, -1 };
    vec expect_b_a = { -5, 1 };

    /* Copy operator */
    vec c = a - b;
    EXPECT_EQ(c, expect_a_b);

    vec d = b - a;
    EXPECT_EQ(d, expect_b_a);

    /* Assign operator */
    a -= b;
    EXPECT_EQ(a, expect_a_b);
}

TYPED_TEST(Basic, scalar_multiplication)
{
    using vec = TypeParam;

    const vec a = { 1, -2 };
    const vec expect = { 3, -6 };

    EXPECT_EQ(a*3, expect);
    EXPECT_EQ(expect, a*3);
}

TYPED_TEST(Basic, scalar_division)
{
    using vec = TypeParam;

    const vec a = { 3, -6 };
    const vec expect = { 1, -2 };

    EXPECT_EQ(a/3, expect);
    EXPECT_EQ(expect, a/3);
}

TYPED_TEST(Basic, unary_minus)
{
    using vec = TypeParam;

    const vec a = { -1, 5 };
    vec expect = { 1, -5 };

    EXPECT_EQ(-a, expect);
}

TYPED_TEST(Basic, dist_sq)
{
    using vec = TypeParam;
    using unit = typename vec::value_type;

    const vec a = { 6, 5 };
    const vec b = { 3, 1 };
    const unit expect = 25;

    auto d1 = dist_sq(a, b);
    auto d2 = dist_sq(b, a);
    ASSERT_EQ(d1, expect);
    ASSERT_EQ(d2, expect);
}

TYPED_TEST(Basic, format)
{
    using vec = TypeParam;

    const vec a = { 3, 1 };
    const auto expect = "{ 3, 1 }"sv;

    auto s = fmt::format("{}", a);
    EXPECT_EQ(s, expect);
}
