#include <gtest/gtest.h>
#include <random>
#include <vector>
#include "util/bitstring.hh"

namespace {
  constexpr size_t N = 10007;

  bool rand_bool()
  {
      static std::mt19937 g(std::random_device {}());
      static std::bernoulli_distribution b(0.5);
      return b(g);
  }

  size_t rand_sizet(size_t max)
  {
      static std::mt19937 g(std::random_device {}());
      return std::uniform_int_distribution<size_t>(0, max - 1)(g);
  }
}

using uti::bitstring;

TEST(Bitstring, declare)
{
    bitstring bs {};
    ASSERT_EQ(0, bs.size());
}

TEST(Bitstring, init_256)
{
    bitstring bs(256);
    ASSERT_EQ(256, bs.size());
}

TEST(Bitstring, assign)
{
    bitstring bs(8);
    ASSERT_EQ(8, bs.size());

    bs[1] = true;
    bs[3] = true;
    bs[4] = true;

    ASSERT_EQ(false, bs[0]);
    ASSERT_EQ(true, bs[1]);
    ASSERT_EQ(false, bs[2]);
    ASSERT_EQ(true, bs[3]);
    ASSERT_EQ(true, bs[4]);
    ASSERT_EQ(false, bs[5]);
    ASSERT_EQ(false, bs[6]);
    ASSERT_EQ(false, bs[7]);
}

TEST(Bitstring, initializer_list)
{
    bitstring bs { false, true, false, true, true, false, false, false };
    ASSERT_EQ(8, bs.size());

    ASSERT_EQ(false, bs[0]);
    ASSERT_EQ(true, bs[1]);
    ASSERT_EQ(false, bs[2]);
    ASSERT_EQ(true, bs[3]);
    ASSERT_EQ(true, bs[4]);
    ASSERT_EQ(false, bs[5]);
    ASSERT_EQ(false, bs[6]);
    ASSERT_EQ(false, bs[7]);
}

TEST(Bitstring, to_string)
{
    bitstring bs { false, true, false, true, true, false, false, false };

    ASSERT_EQ("01011000", uti::to_string(bs));
}

TEST(Bitstring, and)
{
    bitstring bs1 { false, false, false, true, true, false, true, true };
    bitstring bs2 { false, true, false, true, true, false, false, false };

    auto expect = "00011000";
    ASSERT_EQ(expect, uti::to_string(bs1 & bs2));
    bs1 &= bs2;
    ASSERT_EQ(expect, uti::to_string(bs1));
}

TEST(Bitstring, or)
{
    bitstring bs1 { false, false, false, true, true, false, true, true };
    bitstring bs2 { false, true, false, true, true, false, false, false };

    auto expect = "01011011";
    ASSERT_EQ(expect, uti::to_string(bs1 | bs2));
    bs1 |= bs2;
    ASSERT_EQ(expect, uti::to_string(bs1));
}

TEST(Bitstring, xor)
{
    bitstring bs1 { false, false, false, true, true, false, true, true };
    bitstring bs2 { false, true, false, true, true, false, false, false };

    auto expect = "01000011";
    ASSERT_EQ(expect, uti::to_string(bs1 ^ bs2));
    bs1 ^= bs2;
    ASSERT_EQ(expect, uti::to_string(bs1));
}

TEST(Bitstring, xor_count)
{
    bitstring bs1 { false, false, false, true, true, false, true, true };
    bitstring bs2 { false, true, false, true, true, false, false, false };

    ASSERT_EQ(3, bs1.xor_count(bs2));
}

TEST(Bitstring, not)
{
    bitstring bs { false, true, false, true, true, false, false, false };
    ASSERT_EQ("10100111", uti::to_string(~bs));
}

TEST(Bitstring, count)
{
    bitstring bs { false, true, false, true, true, false, false, false };
    ASSERT_EQ(3, bs.count());
}

TEST(Bitstring, not_count)
{
    bitstring bs { false, true, false, true, true, false, false, false };
    ASSERT_EQ(5, (~bs).count());
}

TEST(Bitstring, erase)
{
    bitstring bs { false, true, false, true, true, false, false, false };
    bs.erase(4);
    bs.erase(0);
    bs.erase(5);
    ASSERT_EQ("10100", uti::to_string(bs));
}

TEST(Bitstring, large_and)
{
    size_t n = 10007;
    bitstring bs1(n);
    bitstring bs2(n);
    std::string expect;
    expect.resize(n);

    for (size_t i {}; i < n; ++i) {
        auto a = rand_bool();
        auto b = rand_bool();

        bs1[i] = a;
        bs2[i] = b;
        expect[i] = (a && b) ? '1' : '0';
    }

    ASSERT_EQ(expect, uti::to_string(bs1 & bs2));
    bs1 &= bs2;
    ASSERT_EQ(expect, uti::to_string(bs1));
}

TEST(Bitstring, large_or)
{
    size_t n = 10007;
    bitstring bs1(n);
    bitstring bs2(n);
    std::string expect;
    expect.resize(n);

    for (size_t i {}; i < n; ++i) {
        auto a = rand_bool();
        auto b = rand_bool();

        bs1[i] = a;
        bs2[i] = b;
        expect[i] = (a || b) ? '1' : '0';
    }

    ASSERT_EQ(expect, uti::to_string(bs1 | bs2));
    bs1 |= bs2;
    ASSERT_EQ(expect, uti::to_string(bs1));
}

TEST(Bitstring, large_xor)
{
    size_t n = 10007;
    bitstring bs1(n);
    bitstring bs2(n);
    std::string expect;
    expect.resize(n);

    for (size_t i {}; i < n; ++i) {
        auto a = rand_bool();
        auto b = rand_bool();

        bs1[i] = a;
        bs2[i] = b;
        expect[i] = (a ^ b) ? '1' : '0';
    }

    ASSERT_EQ(expect, uti::to_string(bs1 ^ bs2));
    bs1 ^= bs2;
    ASSERT_EQ(expect, uti::to_string(bs1));
}

TEST(Bitstring, large_xor_count)
{
    bitstring bs1(N);
    bitstring bs2(N);
    size_t expect {};

    for (size_t i {}; i < N; ++i) {
        auto a = rand_bool();
        auto b = rand_bool();

        bs1[i] = a;
        bs2[i] = b;
        expect += (a ^ b) ? 1 : 0;
    }

    ASSERT_EQ(expect, bs1.xor_count(bs2));
}

TEST(Bitstring, large_not)
{
    size_t n = 10007;
    bitstring bs(n);
    std::string expect;
    expect.resize(n);

    for (size_t i {}; i < n; ++i) {
        auto a = rand_bool();

        bs[i] = a;
        expect[i] = !a ? '1' : '0';
    }

    ASSERT_EQ(expect, uti::to_string(~bs));
}

TEST(Bitstring, large_count)
{
    size_t n = 10007;
    bitstring bs(n);
    size_t expect {};

    for (size_t i {}; i < n; ++i) {
        auto a = rand_bool();

        bs[i] = a;
        expect += a ? 1 : 0;
    }

    ASSERT_EQ(expect, bs.count());
}

TEST(Bitstring, large_not_count)
{
    size_t n = 10007;
    bitstring bs(n);
    size_t expect {};

    for (size_t i {}; i < n; ++i) {
        auto a = rand_bool();

        bs[i] = a;
        expect += !a ? 1 : 0;
    }

    ASSERT_EQ(expect, (~bs).count());
}

TEST(Bitstring, large_erase)
{
    size_t n = 10007;
    bitstring bs(n);
    std::string expect {};
    std::vector<size_t> delete_ids;

    for (size_t i {}; i < n; ++i) {
        auto a = rand_bool();
        auto should_delete = rand_bool();

        bs[i] = a;
        expect.push_back(a ? '1' : '0');
        if (should_delete) {
            delete_ids.push_back(i);
        }
    }

    for (auto i : delete_ids) {
        if (i >= expect.size())
            continue;
        expect.erase(expect.begin() + i);
        bs.erase(i);
        break;
    }

    ASSERT_EQ(expect, uti::to_string(bs));
}

TEST(Bitstring, large_erase_all)
{
    bitstring bs(N);
    std::string str;
    str.resize(N);

    for (size_t i {}; i < N; ++i) {
        auto a = rand_bool();
        bs[i] = a;
        str[i] = a ? '1' : '0';
    }

    for (size_t i {}; i < N; ++i) {
        auto which = rand_sizet(bs.size());
        str.erase(str.begin() + which);
        bs.erase(which);
        ASSERT_EQ(str, uti::to_string(bs));
    }

    ASSERT_EQ("", uti::to_string(bs));
}

TEST(Bitstring, iterator)
{
    bitstring bs(N);
    std::string str;
    for (auto x : bs) {
        auto a = rand_bool();
        x = a;
        str.push_back(a ? '1' : '0');
    }

    ASSERT_EQ(str, uti::to_string(bs));
}

TEST(Bitstring, erase_empty)
{
    bitstring bs;
    bs.erase(0);
}
