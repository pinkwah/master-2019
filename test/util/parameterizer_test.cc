#include <gtest/gtest.h>
#include <util/parameterizer.hh>

TEST(Parameterizer, declare)
{
    bool was_called {};

    util::visit_type("int", types_t<int>, [&](auto tag) {
        auto resolve = std::is_same_v<typename decltype(tag)::type, int>;
        EXPECT_TRUE(resolve);
        was_called = true;
    });

    ASSERT_TRUE(was_called);
}

/*
TEST(Parameterizer, multiple_types)
{
    bool was_called {};

    util::parameterizer<0, int, float, double> p;

    p.resolve(0, "float", [&](auto tag) {
        auto resolve = std::is_same_v<typename decltype(tag)::type, float>;
        EXPECT_TRUE(resolve);
        was_called = true;
    });

    ASSERT_TRUE(was_called);
}

TEST(Parameterizer, wrong_type)
{
    bool was_called {};

    util::parameterizer<0, int, double> p;

    try {
        p.resolve(0, "float", [&](auto tag) {
            std::abort();
        });

        std::abort();
    } catch(util::invalid_parameter& e) {
    }

    ASSERT_FALSE(was_called);
}

TEST(Parameterizer, static_size)
{
    bool was_called {};

    util::parameterizer<4, int, double> p;

    p.resolve(2, "double", [&](auto tag) {
        ASSERT_EQ(tag.static_size, 2);
        ASSERT_EQ(tag.size, 2);
        was_called = true;
    });

    ASSERT_TRUE(was_called);
}

TEST(Parameterizer, dynamic_size)
{
    bool was_called {};

    util::parameterizer<4, int, double> p;

    p.resolve(6, "double", [&](auto tag) {
        ASSERT_EQ(tag.static_size, 0);
        ASSERT_EQ(tag.size, 6);
        was_called = true;
    });

    ASSERT_TRUE(was_called);
}

TEST(Parameterizer, return_value)
{
    using namespace std::string_literals;
    bool was_called {};

    util::parameterizer<4, int, double> p;

    auto retval = p.resolve(6, "double", [&](auto tag) {
        EXPECT_EQ(tag.static_size, 0);
        EXPECT_EQ(tag.size, 6);
        was_called = true;
        return "hello"s;
    });

    ASSERT_TRUE(was_called);
    ASSERT_EQ(retval, "hello"s);
}

*/
