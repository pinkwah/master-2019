#include <gtest/gtest.h>
#include <util/result.hh>
#include <util/euclidean/vec.hh>

using vec2i = euclidean::vec<int, 2>;

TEST(Result, empty)
{
    util::result<int> res;
    ASSERT_FALSE(res);
}

TEST(Result, multiple_results)
{
    util::result<int> res;

    res = 3;
    res = 1;
    res = 2;

    ASSERT_EQ(1, res.cost());
}

TEST(Result, custom_cost)
{
    struct cost_fn {
        int operator()(const vec2i& v) {
            return std::inner_product(v.begin(), v.end(), v.begin(), 0);
        }
    };

    util::result<vec2i, cost_fn> res;

    res = vec2i { 2, 15 };
    res = vec2i { 5, 2 };
    res = vec2i { 15, 1 };

    ASSERT_EQ(29, res.cost());
}

TEST(Result, custom_cost_maximize)
{
    struct cost_fn {
        int operator()(const vec2i& v) {
            return std::inner_product(v.begin(), v.end(), v.begin(), 0);
        }
    };

    util::result<vec2i, cost_fn, std::greater<int>> res;

    res = vec2i { 2, 15 };
    res = vec2i { 5, 2 };
    res = vec2i { 15, 1 };

    ASSERT_EQ(229, res.cost());
}

TEST(Result, cheap_cost)
{
    using pair = std::pair<vec2i, int>;
    struct cost_fn {
        int operator()(const pair& p) {
            return p.second;
        }
    };

    util::result<pair, cost_fn> res;

    res = { { 1, 2 }, 3 };
    res = { { 5, 1 }, 1 };
    res = { { 0, 0 }, 2 };

    ASSERT_EQ(1, res.cost());
}
